"""
Forms and validation code for user registration.

"""

import re
from django.contrib.auth.models import User
from django import forms
from django.utils.translation import ugettext_lazy as _

# import gateswap database models
from GateSwap.collegemall.models import School, ROLE_CHOICES, YEAR_CHOICES


# I put this on all required fields, because it's easier to pick up
# on them with CSS or JavaScript if they have a class of "required"
# in the HTML. Your mileage may vary. If/when Django ticket #3515
# lands in trunk, this will no longer be necessary.
attrs_dict = {'class': 'required'}


class RegistrationForm(forms.Form):
    """
    Form for registering a new user account.
    
    Validates that the requested username is not already in use, and
    requires the password to be entered twice to catch typos.
    
    Subclasses should feel free to add any additional validation they
    need, but should avoid defining a ``save()`` method -- the actual
    saving of collected user data is delegated to the active
    registration backend.
    
    """
    username = forms.RegexField(regex=r'^[\w.@+-]+$',
                                max_length=30,
                                widget=forms.TextInput(attrs=attrs_dict),
                                label=_("Username"),
                                error_messages={'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")})
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict,
                                                               maxlength=75)),
                             label=_("Email"))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
                                label=_("Password"))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
                                label=_("Password (again)"))
    
    def clean_username(self):
        """
        Validate that the username is alphanumeric and is not already
        in use.
        
        """
        existing = User.objects.filter(username__iexact=self.cleaned_data['username'])
        if existing.exists():
            raise forms.ValidationError(_("A user with that username already exists."))
        else:
            return self.cleaned_data['username']

    def clean(self):
        """
        Verifiy that the values entered into the two password fields
        match. Note that an error here will end up in
        ``non_field_errors()`` because it doesn't apply to a single
        field.
        
        """
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_("The two password fields didn't match."))
        return self.cleaned_data


class RegistrationFormTermsOfService(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which adds a required checkbox
    for agreeing to a site's Terms of Service.
    
    """
    tos = forms.BooleanField(widget=forms.CheckboxInput(attrs=attrs_dict),
                             label=_(u'I have read and agree to the Terms of Service'),
                             error_messages={'required': _("You must agree to the terms to register")})


class RegistrationFormUniqueEmail(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which enforces uniqueness of
    email addresses.
    
    """
    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.
        
        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("This email address is already in use. Please supply a different email address."))
        return self.cleaned_data['email']


class RegistrationFormNoFreeEmail(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which disallows registration with
    email addresses from popular free webmail services; moderately
    useful for preventing automated spam registrations.
    
    To change the list of banned domains, subclass this form and
    override the attribute ``bad_domains``.
    
    """
    bad_domains = ['aim.com', 'aol.com', 'email.com', 'gmail.com',
                   'googlemail.com', 'hotmail.com', 'hushmail.com',
                   'msn.com', 'mail.ru', 'mailinator.com', 'live.com',
                   'yahoo.com']
    
    def clean_email(self):
        """
        Check the supplied email address against a list of known free
        webmail domains.
        
        """
        email_domain = self.cleaned_data['email'].split('@')[1]
        if email_domain in self.bad_domains:
            raise forms.ValidationError(_("Registration using free email addresses is prohibited. Please supply a different email address."))
        return self.cleaned_data['email']
        

class GateswapRegistrationForm(RegistrationForm):
    # to get the autocomplete box back, uncomment "school" under custom fields and uncomment javascript in registration_bubble_base.html
    cust_attrs = dict(attrs_dict)
    cust_attrs['onchange'] = "javascript:document.getElementById('id_school_id').value = this.value;"

    # Custom fields
    school = forms.ChoiceField(choices=[(0, "----")] + [(school.id, school.name) for school in School.objects.all()], widget=forms.Select(attrs=cust_attrs))
    #school = forms.CharField(widget=forms.TextInput(attrs=dict(attrs_dict, maxlength=150)), label=_("Your School"))
    school_id = forms.IntegerField(widget=forms.HiddenInput())
    tos = forms.BooleanField(widget=forms.CheckboxInput(attrs=attrs_dict),
                             label=_(u'I have read and agree to the Terms of Service'),
                             error_messages={'required': _("You must agree to the terms to register")})
    first_name = forms.CharField(widget=forms.TextInput(attrs=dict(attrs_dict,
                                                               maxlength=75)),
                                                               label=_("First Name"))
    last_name = forms.CharField(widget=forms.TextInput(attrs=dict(attrs_dict,
                                                               maxlength=75)),
                                                               label=_("Last Name"))
    you_are = forms.ChoiceField(choices=ROLE_CHOICES, widget=forms.Select(attrs=attrs_dict))
    year_of_graduation = forms.ChoiceField(choices=[('', '----')]+YEAR_CHOICES, required=False)
                                                               
    def __init__(self, *args, **kw):
        super(RegistrationForm, self).__init__(*args, **kw)
        self.fields.keyOrder = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
            'you_are',
            'year_of_graduation',
            'school',
            'school_id',
            'tos']
                                                               
    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.
        
        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("This email address is already in use. Please supply a different email address."))
        return self.cleaned_data['email']
        
    def clean(self):
        cleaned_data = super(GateswapRegistrationForm, self).clean()
        
        # check to make sure the school info is valid
        school = None
        if "school_id" in self.cleaned_data and "email" in self.cleaned_data:
            try:
                school = School.objects.get(id=self.cleaned_data["school_id"])
            except School.DoesNotExist:
                raise forms.ValidationError(_("The school info provided is invalid. \
                    Make sure you have Javascript turned on when using Gateswap."))
                    
            # check to make given email matches school email suffix
            email_domain = cleaned_data["email"].split('@')[1]
            if school.regex_email_suffix:
                if re.match(school.email_suffix, email_domain) is None:
                    raise forms.ValidationError(_("The email address you've provided \
                        does not match that school's email address format."))
            else: # check for exact match
                if school.email_suffix != email_domain:
                    raise forms.ValidationError(_("The email address you've provided \
                        does not match that school's email address format."))
        
        # give a more useful error for school ID field
        if "school_id" in self._errors:
            del(self._errors["school_id"])
            raise forms.ValidationError(_("The school info provided is invalid. \
                Make sure you have Javascript turned on when using Gateswap."))
                
        # check out the year and role info for compatibility (students must select a year)
        student_id = None
        for choice in ROLE_CHOICES:
            (id, role) = choice
            if role == "a student":
                student_id = str(id)
                break
                
        # if student
        if "you_are" in self.cleaned_data and cleaned_data["you_are"] == student_id:
            if "year_of_graduation" not in self.cleaned_data or not self.cleaned_data['year_of_graduation']:
                raise forms.ValidationError(_("If you are a student, you must provide your year of graduation."))
                                            
        return cleaned_data