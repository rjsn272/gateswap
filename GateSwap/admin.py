from django.contrib.admin.sites import AdminSite
from django.contrib import admin
from django.views.decorators.cache import never_cache
from django.utils.translation import ugettext as _
from django.template.response import TemplateResponse
from django.utils.text import capfirst
from django.core.urlresolvers import reverse, NoReverseMatch
from django.contrib.comments.models import Comment
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.admin.sites import NotRegistered
from GateSwap.collegemall.models import School, UserProfile, Listing, Bid, HelpWanted, LostFoundPosting, Rideshare
from GateSwap.notification import models as n_models
from GateSwap.registration import models as r_models
from django.db import transaction
from django.conf import settings
from django.contrib.auth.forms import (UserCreationForm, UserChangeForm,
    AdminPasswordChangeForm)
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404
from django.utils.html import escape
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext, ugettext_lazy as _
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters

#
# Custom admin site (needed to display administration statistics)
#

class GateswapAdminSite(AdminSite):

    @never_cache
    def index(self, request, extra_context=None):
        """
        Displays the main admin index page, which lists all of the installed
        apps that have been registered in this site.
        """
        app_dict = {}
        user = request.user
        for model, model_admin in self._registry.items():
            app_label = model._meta.app_label
            has_module_perms = user.has_module_perms(app_label)

            if has_module_perms:
                perms = model_admin.get_model_perms(request)

                # Check whether user has any perm for this module.
                # If so, add the module to the model_list.
                if True in perms.values():
                    info = (app_label, model._meta.module_name)
                    model_dict = {
                        'name': capfirst(model._meta.verbose_name_plural),
                        'perms': perms,
                    }
                    if perms.get('change', False):
                        try:
                            model_dict['admin_url'] = reverse('admin:%s_%s_changelist' % info, current_app=self.name)
                        except NoReverseMatch:
                            pass
                    if perms.get('add', False):
                        try:
                            model_dict['add_url'] = reverse('admin:%s_%s_add' % info, current_app=self.name)
                        except NoReverseMatch:
                            pass
                    if app_label in app_dict:
                        app_dict[app_label]['models'].append(model_dict)
                    else:
                        app_dict[app_label] = {
                            'name': app_label.title(),
                            'app_url': reverse('admin:app_list', kwargs={'app_label': app_label}, current_app=self.name),
                            'has_module_perms': has_module_perms,
                            'models': [model_dict],
                        }

        # Sort the apps alphabetically.
        app_list = list(app_dict.itervalues())
        app_list.sort(key=lambda x: x['name'])

        # Sort the models alphabetically within each app.
        for app in app_list:
            app['models'].sort(key=lambda x: x['name'])

        # CUSTOM: Build site statistics
        # note: we try to do this in the most intelligent way possible to minimize queries
        # we may or may not succeed
        stats_dict = {}

        ### users
        total_by_school = {}
        for school in School.objects.all():
            total_by_school[school.name] = UserProfile.objects.filter(school=school).count()

        stats_dict["user"] = {
            'total': User.objects.count(),
            'total_by_school': total_by_school,
        }

        ### listings
        listings_total = Listing.objects.count()
        helpwanted_total = HelpWanted.objects.count()
        rideshare_total = Rideshare.objects.count()
        lostfound_total = LostFoundPosting.objects.count()
        all_listing_total = listings_total + helpwanted_total + rideshare_total + lostfound_total

        completed_total = 0
        for l_type in [Listing, HelpWanted, Rideshare, LostFoundPosting]:
            completed_total += l_type.objects.filter(fulfilled=True).count()

        listing_types = {
            "College Mall": listings_total,
            "Help Wanted": helpwanted_total,
            "Rideshare": rideshare_total,
            "Lost and Found": lostfound_total,
        }

        stats_dict["listings"] = {
            'total': all_listing_total,
            'total_by_type': listing_types,
            'total_completed': completed_total,
        }

        ### responses
        comment_total = Comment.objects.count()
        comment_per_listing = comment_total / float(all_listing_total) if float(all_listing_total) else 0

        bid_total = Bid.objects.count()
        selling_items_total = Listing.objects.filter(selling=True).count()
        bid_per_item = bid_total / float(selling_items_total) if float(selling_items_total) else 0

        stats_dict["responses"] = {
            "comment_total": comment_total,
            "comment_per_listing": comment_per_listing,
            "bid_total": bid_total,
            "bid_per_item": bid_per_item,
        }

        ### END STATISTICS ###

        context = {
            'title': _('Site administration'),
            'app_list': app_list,
            'stats_dict': stats_dict,
        }
        context.update(extra_context or {})
        return TemplateResponse(request, [
            self.index_template or 'admin/index.html',
        ], context, current_app=self.name)

csrf_protect_m = method_decorator(csrf_protect)

site = GateswapAdminSite()

# do all the necessary re-registration to have django admin stuff show up in our new admin site
try:
    site.unregister(User)
except NotRegistered:
    pass    
site.register(User, UserAdmin)
try:
    site.unregister(Group)
except NotRegistered:
    pass
site.register(Group, GroupAdmin)
site.register(Comment)