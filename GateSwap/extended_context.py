# extended_context.py
from datetime import datetime
from GateSwap.notification import models as notification
from GateSwap.mycampus.models import Event
from GateSwap.collegemall.models import Category, Listing, UserProfile

def user(request):
    if hasattr(request, 'user'):
        if request.user.is_authenticated():
            notification_count = notification.Notice.objects.unseen_count_for(request.user)
            event_ticker = None
            try:
                event_ticker = Event.objects.filter(
                startdate__gte=datetime.now(),
                school=request.user.get_profile().school
                ).order_by('startdate')[:10]
            except UserProfile.DoesNotExist:
                pass

            categories = []
            # attach count to each category
            for category in Category.objects.all():
                count = Listing.objects.filter(category=category, fulfilled=False).count()
                categories.append((category, count))
            return {'user':request.user, 'n_count': notification_count, 'event_ticker': event_ticker, 'categories': categories}
    return {}