from django.utils.translation import ugettext_noop as _
from django.db.models import signals
from GateSwap import settings


if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification

    def create_notice_types(**kwargs):
        notification.create_notice_type("new_comment", _("New Comment on Your Listing"), _("Your listing has received a new comment."))
        notification.create_notice_type("new_bid", _("New Bid on Your Listing"), _("Your listing has received a new bid."))
        notification.create_notice_type("outbidded", _("You have been outbidded!"), _("Someone has made a higher bid than you."))

    signals.post_syncdb.connect(create_notice_types, sender=notification)
else:
    print "Skipping creation of NoticeTypes as notification app not found"