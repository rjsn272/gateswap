import StringIO, uuid
from PIL import Image
from django.core.files.base import ContentFile
from GateSwap import settings

#### File Renamer ####

def generate_filename(filename):
    ext = filename.split('.')[-1]
    return "%s.%s" % (uuid.uuid4(), ext)

#### Thumbnail Generators ####
# based on https://github.com/pcraciunoiu/kitsune/tree/

def create_thumbnail(file, longest_side=settings.THUMBNAIL_SIZE):
    """
    Returns a thumbnail file with a set longest side.
    """
    originalImage = Image.open(file)
    originalImage = originalImage.convert("RGB")
    (file_width, file_height) = originalImage.size

    (width, height) = _scale_dimensions(file_width, file_height, longest_side)
    resizedImage = originalImage.resize((width, height), Image.ANTIALIAS)

    io = StringIO.StringIO()
    resizedImage.save(io, 'PNG')

    return ContentFile(io.getvalue())


def _scale_dimensions(width, height, longest_side=settings.THUMBNAIL_SIZE):
    """
    Returns a tuple (width, height), both smaller than longest side, and
    preserves scale.
    """

    if width < longest_side and height < longest_side:
        return (width, height)

    if width > height:
        new_width = longest_side
        new_height = (new_width * height) / width
        return (new_width, new_height)

    new_height = longest_side
    new_width = (new_height * width) / height
    return (new_width, new_height)