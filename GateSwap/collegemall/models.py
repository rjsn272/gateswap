import datetime
from random import randint
from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.contrib.comments.signals import comment_was_posted
from django.db.models.signals import post_save
from django.dispatch import receiver
from GateSwap import settings
from GateSwap.notification import models as notification
from mailchimp import utils


class Category(models.Model):
    name = models.CharField(max_length=100)
    def __unicode__(self):
        return self.name

class ImageAttachment(models.Model):
    file = models.ImageField(upload_to=settings.IMAGE_UPLOAD_PATH)
    thumbnail = models.ImageField(upload_to=settings.THUMBNAIL_UPLOAD_PATH)
    creator = models.ForeignKey(User, related_name='image_attachments')
    def __unicode__(self):
        return self.file.name
    
class School(models.Model):
    name = models.CharField(max_length=100)
    bg_photo = models.ForeignKey(ImageAttachment)
    email_suffix = models.CharField(max_length=60)
    regex_email_suffix = models.BooleanField(help_text="Is the e-mail suffix a regex?")
    def __unicode__(self):
        return self.name
    
ROLE_CHOICES = (
    (0, 'a student'),
    (1, 'a professor'),
    (2, 'other faculty'),
)

YEAR_CHOICES = list(
    (year, year) for year in range(2013, 2017)
)

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    school = models.ForeignKey(School)
    role = models.IntegerField(choices=ROLE_CHOICES)
    year = models.IntegerField(choices=YEAR_CHOICES, blank=True, null=True)
    def __unicode__(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)

class RandomManager(models.Manager):
    def random(self, quantity=1):
        count = self.aggregate(count=models.Count('id'))['count']
        rand_list = []
        
        if count > 0:
            for i in range(quantity):
                random_index = randint(0, count - 1)
                item = self.all()[random_index]
                
                # make sure our featured listings have pictures
                while (self.model is Listing or self.model is LostFoundPosting) and item.image is None:
                    random_index = randint(0, count - 1)
                    item = self.all()[random_index]
                
                rand_list.append(item)
        
        return rand_list
        
class Listing(models.Model):
    objects = RandomManager()
    MODELNAME = "LISTING"
    title = models.CharField(max_length=140)
    pub_date = models.DateTimeField(default=datetime.datetime.now())
    owner = models.ForeignKey(User)
    category = models.ForeignKey(Category)
    school = models.ForeignKey(School)
    description = models.CharField(max_length=1000)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    image = models.ForeignKey(ImageAttachment, blank=True, null=True)
    selling = models.BooleanField()
    fulfilled = models.BooleanField(default=False)
    def __unicode__(self):
        return self.title

# for Listing model        
class Bid(models.Model):
    amount = models.DecimalField(max_digits=6, decimal_places=2)
    made_by = models.ForeignKey(User)
    pub_date = models.DateTimeField(default=datetime.datetime.now())
    listing = models.ForeignKey(Listing)
    def __unicode__(self):
        return "$" + str(self.amount)
        
class HelpWanted(models.Model):
    objects = RandomManager()
    MODELNAME = "HELP_WANTED"
    title = models.CharField(max_length=140)
    pub_date = models.DateTimeField(default=datetime.datetime.now())
    owner = models.ForeignKey(User)
    school = models.ForeignKey(School)
    description = models.CharField(max_length=1000)
    time_required = models.CharField(max_length=140)
    compensation = models.CharField(max_length=140, blank=True, null=True)
    fulfilled = models.BooleanField(default=False)
    def __unicode__(self):
        return self.title
        
class LostFoundCategory(models.Model):
    name = models.CharField(max_length=100)
    def __unicode__(self):
        return self.name

class LostFoundPosting(models.Model):
    objects = RandomManager()
    MODELNAME = "LOST_FOUND"
    title = models.CharField(max_length=140)
    pub_date = models.DateTimeField(default=datetime.datetime.now())
    owner = models.ForeignKey(User)
    lost = models.BooleanField()
    category = models.ForeignKey(LostFoundCategory)
    school = models.ForeignKey(School)
    description = models.CharField(max_length=1000)
    place_lf = models.CharField("Where?", max_length=140)
    date_lf = models.DateField("When did you find/lose?")
    reward = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    image = models.ForeignKey(ImageAttachment, blank=True, null=True)
    fulfilled = models.BooleanField(default=False)
    def __unicode__(self):
        return self.title
    
class Rideshare(models.Model):
    objects = RandomManager()
    MODELNAME = "RIDESHARE"
    to_location = models.CharField("From Where?", max_length=140)
    from_location = models.CharField("To Where?", max_length=140)
    giving_ride = models.BooleanField()
    details = models.CharField(max_length=1000, blank=True, null=True)
    pub_date = models.DateTimeField(default=datetime.datetime.now())
    owner = models.ForeignKey(User)
    school = models.ForeignKey(School)
    date = models.DateField("When?")
    time = models.TimeField(blank=True, null=True)
    cost = models.DecimalField("How much?", max_digits=6, decimal_places=2)
    fulfilled = models.BooleanField(default=False)
    def __unicode__(self):
        return self.to_location
        
        
##### SIGNAL REGISTRATION/SETUP ####
def send_comment_notification(sender, **kwargs):
    comment = kwargs['comment']
    recipients = [comment.content_object.owner]
    listing = comment.content_object
    context = {'comment': comment, 'listing': listing}
    notification.send(recipients, 'new_comment', extra_context=context, content_object=comment)
comment_was_posted.connect(send_comment_notification)

@receiver(post_save, sender=School)
def create_new_mailing_list(sender, instance, created, **kwargs):
    if created:
        gateswap_list = utils.get_connection().get_list_by_id(settings.MAILCHIMP_LIST_ID)
        gateswap_list.add_interest_group(instance.name, settings.MAILCHIMP_GROUPING_ID)


# Due to this issue (http://stackoverflow.com/questions/3430937/connecting-a-django-registration-signal)
# and my desire to make the Gateswap codebase completely portable, we are unable to connect to the
# user_activated signal from django-registration. This results in potentially inactive users
# receiving Gateswap email campaigns.
# @ksolan 1/19/13
@receiver(post_save, sender=UserProfile)
def add_email_subscriber(sender, instance, created, **kwargs):
    if created:    
        user = instance.user
        gateswap_list = utils.get_connection().get_list_by_id(settings.MAILCHIMP_LIST_ID)
        gateswap_list.subscribe(user.email, {
            'EMAIL': user.email, 
            'FNAME': user.first_name, 
            'LNAME': user.last_name,
            'GROUPINGS': [{
                            'id': settings.MAILCHIMP_GROUPING_ID, 
                            'groups': instance.school.name
                         }]
        })