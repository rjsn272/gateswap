import datetime, re
from django import forms
from django.forms import extras
from django.core.exceptions import ValidationError
from django.core.files import File
from django.db.models import Max
from django.contrib.auth import authenticate
from GateSwap.collegemall import models, helpers, custom_widget
from GateSwap import settings
from GateSwap.notification import models as notification
                    
    
class PasscodeForm(forms.Form):
    passcode = forms.CharField(widget=forms.PasswordInput)

class SuggestSchoolForm(forms.Form):
    school = forms.CharField(widget=forms.TextInput(
        attrs={'value':"College, university, etc.", 
               'title':"College, university, etc."}
        )
    )
    
class LoginForm(forms.Form):
    username = forms.CharField()
    username.widget = forms.TextInput(attrs={'value':"Email", 'class':'username_field', 'title': "Email"})
    password = forms.CharField()
    password.widget = forms.PasswordInput(attrs={'value':"Password", 'class':"password_field", 'title': "Password"})
    
    error_messages = {
        'invalid_login': ("Please enter a correct email and password. "
                           "Note that both fields are case-sensitive."),
        'no_cookies': ("Your Web browser doesn't appear to have cookies "
                        "enabled. Cookies are required for logging in."),
        'inactive': ("This account is inactive."),
    }
    
    def __init__(self, request=None, *args, **kwargs):
        """
        If request is passed in, the form will validate that cookies are
        enabled. Note that the request (a HttpRequest object) must have set a
        cookie with the key TEST_COOKIE_NAME and value TEST_COOKIE_VALUE before
        running this validation.
        """
        self.request = request
        self.user_cache = None
        super(LoginForm, self).__init__(*args, **kwargs)
    
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
    
        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'])
            elif not self.user_cache.is_active:
                raise forms.ValidationError(self.error_messages['inactive'])
        self.check_for_test_cookie()
        return self.cleaned_data
    
    def check_for_test_cookie(self):
        if self.request and not self.request.session.test_cookie_worked():
            raise forms.ValidationError(self.error_messages['no_cookies'])
    
    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None
    
    def get_user(self):
        return self.user_cache
        
class ListingForm(forms.ModelForm):
    selling_or_looking_for = forms.ChoiceField(choices=((False, "Looking For"), (True, "Selling")), widget=forms.Select(attrs={'class': 'createlisting_form_category'}))
    image_of_item = forms.ImageField(required=False)
    type = forms.CharField(widget=forms.HiddenInput(), initial="listing")
    
    class Meta:
        model = models.Listing
        fields = ('title', 'selling_or_looking_for', 'description', 'category', 'price')
        widgets = {
            'title': forms.TextInput(attrs={'class': 'createlisting_field'}),
            'category': forms.Select(attrs={'class': 'createlisting_form_category'}),
            'description': forms.Textarea,
            'price': forms.TextInput(attrs={'class': 'createlisting_field'}),
        }
    
    def __init__(self, *args, **kwargs):
         self.user = kwargs.pop('user',None)
         self.image_file = kwargs.pop('image_file',None)
         super(ListingForm, self).__init__(*args, **kwargs)
            
    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(ListingForm, self).save(commit=False)
        
        # add in the current user info to the model
        m.owner = self.user
        m.school = self.user.get_profile().school

        # handle selling/buying formbox
        m.selling = (self.cleaned_data.get("selling_or_looking_for") == "True")
                
        if self.image_file: # handle image upload
            
            self.image_file.name = helpers.generate_filename(self.image_file.name)
                        
            # thumbnail generation
            thumb_file = helpers.create_thumbnail(self.image_file.file) 
            thumb_file = File(thumb_file) # turn it into a django.core.files.File object
            thumb_file.name = helpers.generate_filename("blank.jpg")
            
            # assign the thumbnail file to the database model and save it
            image_model = models.ImageAttachment(creator=self.user)
            image_model.file = self.image_file
            image_model.thumbnail = thumb_file
            image_model.save()
            m.image = image_model
        
        if commit:
            m.save()
        return m
        
    
class HelpWantedForm(forms.ModelForm):
    type = forms.CharField(widget=forms.HiddenInput(), initial="helpwanted")
    
    class Meta:
        model = models.HelpWanted
        fields = ('title', 'description', 'time_required', 'compensation')
        widgets = {
            'title': forms.TextInput(attrs={'class': 'createlisting_field'}),
            'description': forms.Textarea,
            'time_required': forms.TextInput(attrs={'class': 'createlisting_field'}),
            'compensation': forms.TextInput(attrs={'class': 'createlisting_field'}),
        }
    
    def __init__(self, *args, **kwargs):
         self.user = kwargs.pop('user',None)
         super(HelpWantedForm, self).__init__(*args, **kwargs)
            
    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(HelpWantedForm, self).save(commit=False)
        
        # add in the current user info to the model
        m.owner = self.user
        m.school = self.user.get_profile().school
        
        if commit:
            m.save()
        return m
    
class LostFoundForm(forms.ModelForm):
    lost_or_found = forms.ChoiceField(choices=((True, "Lost"), (False, "Found")), widget=forms.Select(attrs={'class': 'createlisting_form_category'}))
    image_of_item = forms.ImageField(required=False)
    type = forms.CharField(widget=forms.HiddenInput(), initial="lostfoundposting")
    
    class Meta:
        model = models.LostFoundPosting
        fields = ('title', 'lost_or_found', 'category', 'description', 'date_lf', 'place_lf', 'image_of_item', 'reward')
        widgets = {
            'title': forms.TextInput(attrs={'class': 'createlisting_field'}),
            'description': forms.Textarea,
            'category': forms.Select(attrs={'class': 'createlisting_form_category'}),
            'date_lf': extras.SelectDateWidget(attrs={'class': 'createlisting_form_category'}),
            'place_lf': forms.TextInput(attrs={'class': 'createlisting_field'}),
            'reward': forms.TextInput(attrs={'class': 'createlisting_field'}),
        }
    
    def __init__(self, *args, **kwargs):
         self.user = kwargs.pop('user',None)
         self.image_file = kwargs.pop('image_file',None)
         super(LostFoundForm, self).__init__(*args, **kwargs)
            
    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(LostFoundForm, self).save(commit=False)
        
        # add in the current user info to the model
        m.owner = self.user
        m.school = self.user.get_profile().school
            
        # handle lost or found dropdown
        m.lost = (self.cleaned_data.get("lost_or_found") == "True")
        
        if self.image_file: # handle image upload
            
            self.image_file.name = helpers.generate_filename(self.image_file.name)
                        
            # thumbnail generation
            thumb_file = helpers.create_thumbnail(self.image_file.file) 
            thumb_file = File(thumb_file) # turn it into a django.core.files.File object
            thumb_file.name = helpers.generate_filename("blank.png")
            
            # assign the thumbnail file to the database model and save it
            image_model = models.ImageAttachment(creator=self.user)
            image_model.file = self.image_file
            image_model.thumbnail = thumb_file
            image_model.save()
            m.image = image_model
        
        if commit:
            m.save()
        return m
    
    
class RideshareForm(forms.ModelForm):
    type = forms.CharField(widget=forms.HiddenInput(), initial="rideshare")
    giving_or_looking_for_ride = forms.ChoiceField(choices=((False, "Looking For"), (True, "Giving")), widget=forms.Select(attrs={'class': 'createlisting_form_category'}))
    
    class Meta:
        model = models.Rideshare
        fields = ('from_location', 'to_location', 'date', 'time', 'cost', 'details')
        widgets = {
            'from_location': forms.TextInput(attrs={'class': 'createlisting_field'}),
            'to_location': forms.TextInput(attrs={'class': 'createlisting_field'}),
            'date': extras.SelectDateWidget(attrs={'class': 'createlisting_form_category'}),
            'time': custom_widget.SelectTimeWidget(attrs={'class': 'createlisting_form_category'}, twelve_hr=True, use_seconds=False),
            'cost': forms.TextInput(attrs={'class': 'createlisting_field'}),
            'details': forms.Textarea,
        }
    
    def __init__(self, *args, **kwargs):
         self.user = kwargs.pop('user', None)
         super(RideshareForm, self).__init__(*args, **kwargs)
            
    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(RideshareForm, self).save(commit=False)
        
        m.giving_ride = (self.cleaned_data.get("giving_or_looking_for_ride") == "True")
        
        # add in the current user info to the model
        m.owner = self.user
        m.school = self.user.get_profile().school
        
        if commit:
            m.save()
        return m
        
class BidForm(forms.ModelForm):
    class Meta:
        model = models.Bid
        fields = ('amount',)
        widgets = {'amount': forms.TextInput(attrs={'class': 'item_bid'})}
    
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.listing = kwargs.pop('listing', None)
        super(BidForm, self).__init__(*args, **kwargs)
        
    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(BidForm, self).save(commit=False)
        
        # we need to get the current highest bid amount.
        agg = models.Bid.objects.filter(listing=self.listing).aggregate(Max('amount'))
        max = agg["amount__max"]

        if max is not None:
            # if this bid beats old highest bid, notify the former highest bidder(s) that they've been outbidded :'(
            if m.amount > max: 
                # get the group of former highest bidders            
                high_bid_grp = models.Bid.objects.filter(listing=self.listing, amount=max) 
                
                # go through the bids and notify users of outbidding
                recipients = []
                for bid in high_bid_grp:
                    # don't want to notify if the highest bidder is just raising his/her bid
                    if bid.made_by.id != m.made_by.id:
                        recipients.append(bid.made_by)
                if recipients:
                    notification.send(recipients, "outbidded", 
                        extra_content={"old_amount": max, "new_amount": m.amount, 
                            'made_by': self.user, 'listing': self.listing},
                        on_site=True, content_object=self.listing)            
        
        # check if this user has already made a bid on this listing
        try:
            old_bid = models.Bid.objects.get(made_by=self.user, listing=self.listing)
            old_bid.amount = m.amount
            m = old_bid
        except models.Bid.DoesNotExist: # new bid
            # add in the current user info to the model
            m.made_by = self.user
            
            # connect bid to appropriate listing
            m.listing = self.listing
            
        # update publishing date
        m.pub_date = datetime.datetime.now()
        
        if commit:
            m.save()
            
        # send notification
        recipients = [m.listing.owner]
        notification.send(recipients, "new_bid", {"bid": m, "listing": m.listing}, on_site=True, content_object=m)
        
        return m
        
