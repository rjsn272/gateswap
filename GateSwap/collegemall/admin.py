from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.contrib.comments.models import Comment
from GateSwap.admin import site
from GateSwap.collegemall import models

class ListingAdmin(admin.ModelAdmin):
    list_display = ('title', 'pub_date', 'category')
    
class CategoryAdmin(admin.ModelAdmin):
    pass
    
class SchoolAdmin(admin.ModelAdmin):
    pass
    
class UserProfileAdmin(admin.ModelAdmin):
    pass

    
site.register(models.Listing, ListingAdmin)
site.register(models.HelpWanted)
site.register(models.LostFoundPosting)
site.register(models.Rideshare)
site.register(models.Category)
site.register(models.LostFoundCategory)
site.register(models.School)
site.register(models.UserProfile)
site.register(models.ImageAttachment)