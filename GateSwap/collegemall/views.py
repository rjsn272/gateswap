from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views
from django.contrib import comments
from django.http import HttpResponse
from django.db.models import get_model, Q
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from GateSwap import settings
from GateSwap.collegemall import forms, models
from GateSwap.notification import models as notification

def home(request):
    if request.user.is_authenticated():
        listings = models.Listing.objects.filter(
                school__pk=request.user.get_profile().school.id,
                fulfilled=False
        ).order_by('-pub_date')[:7]
        return render(request, "homepage.html", {'listings': listings})
    else:
        return render(request, "welcome.html", { 'form': forms.LoginForm(), 'schools': models.School.objects.all(), 'suggest_form': forms.SuggestSchoolForm() }) # YOU CANNOT ADD TO THIS CONTEXT WITHOUT ADDING TO EXTRA_CONTEXT IN URLS.PY! IT WILL NOT SHOW UP IN THE LOGIN VIEW

def login(request, *args, **kwargs):
    if request.method == 'POST':
        if request.POST.has_key('remember_me'):
            request.session.set_expiry(3600 * 24 * 180) # 180 days
    return auth_views.login(request, *args, **kwargs)

def suggest_new_school(request):
    if request.method == "POST":
        form = forms.SuggestSchoolForm(request.POST)
        if form.is_valid():
            school = form.cleaned_data['school']
            send_mail("New School Requested", 
                "The following school was requested:\n\n{0}\n\nThanks,\nGateswap.com\n".format(school),
                settings.DEFAULT_FROM_EMAIL,
                ['rcarroll@colgate.edu']
            )
            return render(request, "short_message.html",
                {'headline': 'School Submitted',
                'message': "Your school was submitted! We'll review your request, and Gateswap might show up at your school soon!"}
            )
        else:
            return render(request, "welcome.html", { 'form': forms.LoginForm(), 'schools': models.School.objects.all(), 'suggest_form': forms.SuggestSchoolForm(request) })
    else:
        return render(request, "short_message.html",
            {'headline': 'Invalid Request',
            'message': 'Your request was invalid.'}
        )
        
@login_required
def create_listing(request, type=None, pk=None):
        
    if request.method == "POST": # submission
        submit_type = request.POST.get("type", None)
        form_dict = {
            "list_frm": forms.ListingForm(), 
            "hw_frm": forms.HelpWantedForm(), 
            "lf_frm": forms.LostFoundForm(), 
            "rs_frm": forms.RideshareForm(),
        }
        form = None
        if submit_type == "listing":
            form = forms.ListingForm(request.POST, request.FILES, user=request.user, image_file=request.FILES.get("image_of_item", None))
            form_dict["list_frm"] = form
        elif submit_type == "helpwanted": 
            form = forms.HelpWantedForm(request.POST, user=request.user)
            form_dict["hw_frm"] = form
        elif submit_type == "lostfoundposting":
            form = forms.LostFoundForm(request.POST, request.FILES, user=request.user, image_file=request.FILES.get("image_of_item", None))
            form_dict["lf_frm"] = form
        elif submit_type == "rideshare":
            form = forms.RideshareForm(request.POST, user=request.user)
            form_dict["rs_frm"] = form
        else:
            return render(request, "short_message.html", 
                {'headline': "Invalid Form",
                'message': "You submitted an invalid form."}
            )
        
        if form.is_valid():
            form.save()
            return render(request, "short_message.html",
                {'headline': "Submitted Listing",
                'message': "You successfully submitted your listing to Gateswap!"}
            )
        else: # invalid form
            form_dict["type"] = submit_type
            return render(request, "create_listing.html", form_dict)

    else:
        return render(request, "create_listing.html",
            {"list_frm": forms.ListingForm(), 
             "hw_frm": forms.HelpWantedForm(), 
             "lf_frm": forms.LostFoundForm(), 
             "rs_frm": forms.RideshareForm(),}
        )
    
@login_required
def edit_listing(request, type, pk):
    edit_object = None
    edit_model = None
    
    # get the object and check permissions
    if type and pk:
        edit_model = get_model('collegemall', type)
        edit_object = get_object_or_404(edit_model, pk=pk)
        if edit_object.owner != request.user:
            return render(request, "short_message.html", 
                {'headline': "Access Denied",
                'message': "You do not have permission to edit this listing."}
            )
            
    form_dict = {"list_frm": forms.ListingForm(), 
             "hw_frm": forms.HelpWantedForm(), 
             "lf_frm": forms.LostFoundForm(), 
             "rs_frm": forms.RideshareForm(),
             "edit": True,
             "type": type
    }
    
    if request.method == "POST":
        form = None
        if type == "listing":
            form = forms.ListingForm(request.POST, request.FILES, instance=edit_object, user=request.user, image_file=request.FILES.get("image_of_item", None))
            form_dict["list_frm"] = form
        elif type == "helpwanted": 
            form = forms.HelpWantedForm(request.POST, instance=edit_object, user=request.user)
            form_dict["hw_frm"] = form
        elif type == "lostfoundposting":
            form = forms.LostFoundForm(request.POST, request.FILES, instance=edit_object, user=request.user, image_file=request.FILES.get("image_of_item", None))
            form_dict["lf_frm"] = form
        elif type == "rideshare":
            form = forms.RideshareForm(request.POST, instance=edit_object, user=request.user)
            form_dict["rs_frm"] = form
        else: 
            return render(request, "short_message.html", 
                {'headline': "Invalid Form",
                'message': "You submitted an invalid form.",
                'backlink': reverse('market')}
            )
        
        if form.is_valid():
            form.save()
            return render(request, "short_message.html",
                {'headline': "Edited Listing",
                'message': "You successfully edited your listing!",
                'backlink': reverse("my_life")
                }
            )
        else: # invalid form
            return render(request, "create_listing.html", form_dict)
    else: # request.method == "GET"
        
        # create form
        if type == "listing":
            initial_values = {
                'image_of_item': edit_object.image.file if edit_object.image else None, 
                'selling_or_looking_for': edit_object.selling
            }
            form = forms.ListingForm(instance=edit_object, initial=initial_values)
            form_dict["list_frm"] = form
        elif type == "helpwanted": 
            form = forms.HelpWantedForm(instance=edit_object)
            form_dict["hw_frm"] = form
        elif type == "lostfoundposting":
            initial_values = {
                'image_of_item': edit_object.image.file if edit_object.image else None,
                'lost_or_found': edit_object.lost
            }
            form = forms.LostFoundForm(instance=edit_object, initial=initial_values)
            form_dict["lf_frm"] = form
        elif type == "rideshare":
            initial_values = {
                'giving_or_looking_for_ride': edit_object.giving_ride
            }
            form = forms.RideshareForm(instance=edit_object, initial=initial_values)
            form_dict["rs_frm"] = form
        else: 
            return render(request, "short_message.html", 
                {'headline': "Invalid Form",
                'message': "You submitted an invalid form."}
            )
        
        return render(request, "create_listing.html", form_dict)
    
@login_required
def market(request):
    listings = None
    
    categories = []
    # attach count to each category
    for category in models.Category.objects.all():
        count = models.Listing.objects.filter(category=category, fulfilled=False).count()
        categories.append((category, count))
    
    # filter by category or search query (or both), if given. else, return all
    try:
        category_id = request.GET.get('category', None)
        category_filter = None
        search_query = request.GET.get('q', None)
        if category_id and search_query:
            listings = models.Listing.objects.filter(
                title__icontains=search_query,
                category__pk=category_id,
                school__pk=request.user.get_profile().school.id,
                fulfilled=False
            ).order_by('-pub_date')
            try:
                category_filter = models.Category.objects.get(id=category_id)
            except models.Category.DoesNotExist: pass
        elif category_id:
            listings = models.Listing.objects.filter(
                category__pk=category_id, 
                school__pk=request.user.get_profile().school.id,
                fulfilled=False
            ).order_by('-pub_date')
            try:
                category_filter = models.Category.objects.get(id=category_id)
            except models.Category.DoesNotExist: pass
        elif search_query:
            listings = models.Listing.objects.filter(
                Q(title__icontains=search_query) | Q(description__icontains=search_query) | Q(category__name__icontains=search_query),
                school__pk=request.user.get_profile().school.id,
                fulfilled=False
            )
        else:
            listings = models.Listing.objects.filter(
                school__pk=request.user.get_profile().school.id,
                fulfilled=False
            ).order_by('-pub_date')
    except models.Listing.DoesNotExist:
        pass # no listings available
        
    # set up pagination (https://docs.djangoproject.com/en/1.4/topics/pagination/)
    paginator = Paginator(listings, 7) # show 7 listings per page
    page = request.GET.get('page', None)
    try:
        listings = paginator.page(page)
    except PageNotAnInteger:
        listings = paginator.page(1)
    except EmptyPage:
        # if page is out of range, deliver last page
        listings = paginator.page(paginator.num_pages)
        
    # get featured listing (just pull a random one)
    featured = models.Listing.objects.random()
    
    return render(request, "market.html", {'listings': listings, 'categories': categories, 
                                           'featured_list': featured, 'category_filter': category_filter,
                                           'search_query': search_query})
    
@login_required
def market_single(request, pk):
    if request.method == "POST": # handle bid
        try:
            listing=models.Listing.objects.get(id=pk, 
                    school__pk=request.user.get_profile().school.id)
            
            # check to make sure a sneaky seller isn't posting bids to their own item
            # see: http://pages.ebay.com/help/policies/seller-shill-bidding.html
            if request.user.id == listing.owner.id:
                return render(request, "short_message.html", 
                    {'headline': "Fraudulent bid detected.",
                    'message': "Attempting to post a bid to your own item violates \
                                Gateswap's terms and conditions. This action has been \
                                reported."
                    }
                )
            
            bid_form = forms.BidForm(request.POST, user=request.user, listing=listing)
            if bid_form.is_valid():
                bid_form.save()
                return render(request, "short_message.html", 
                    {'headline': "Success!",
                    'message': "Your bid was successfully submitted!",
                    'backlink': reverse('market_single', args=[pk])}
                )
            else:
                return render(request, "market_single.html", 
                    {'listing': listing, 'bid_form': forms.BidForm(request.POST)})
        except models.Listing.DoesNotExist:
            return render(request, "short_message.html", 
                {'headline': "Invalid Bid",
                'message': "You sent a bid to an invalid listing"}
            )
    else: # handle standard view
        max_bid = None
        bids = None
        listing = None
        try:
            # check both ID and school to disallow access to other school's listings
            listing = models.Listing.objects.get(
                id=pk, school__pk=request.user.get_profile().school.id)
            # get bids for this listing
            bids = models.Bid.objects.filter(listing__pk=pk).order_by('amount')
            if bids: 
                max_bid = bids[0]
        except models.Listing.DoesNotExist:
            # if it's a nonexistent view, dump them back to the marketplace
            return redirect("GateSwap.collegemall.views.market")
        
        # mark notification as seen
        notices = notification.Notice.objects.notices_for(request.user, unseen=True)
        for notice in notices:
            if notice.notice_type.label == "new_comment":
                if notice.content_object.content_object == listing:
                    notice.unseen = False
                    notice.save()
            elif notice.notice_type.label == "new_bid":
                if notice.content_object.listing == listing:
                    notice.unseen = False
                    notice.save()
            elif notice.notice_type.label == "outbidded":
                if notice.content_object == listing:
                    notice.unseen = False
                    notice.save()
        
        # we're good to go
        if request.user == listing.owner: # give them the special admin panel
            return render(request, "listing_admin.html",
                {'listing': listing, 'bids': bids, 'max_bid': max_bid})
        else:
            return render(request, "market_single.html", 
                {'listing': listing, 'bid_form': forms.BidForm(), 'max_bid': max_bid})
                
@login_required
def listing_admin(request, pk):
    listing = models.Listing.objects.get(id=pk)
    return render(request, "listing_admin.html")
        
@login_required
def services(request):
    help, lost_found, rideshare = None, None, None
    # get help wanted requests
    try:
        help = models.HelpWanted.objects.filter(
            school__pk=request.user.get_profile().school.id,
            fulfilled=False
        ).order_by('-pub_date')
    except models.HelpWanted.DoesNotExist:
        raise Exception("Empty helpwanted") # no services
    # get lost & found
    try:
        lost_found = models.LostFoundPosting.objects.filter(
            school__pk=request.user.get_profile().school.id,
            fulfilled=False
        ).order_by('-pub_date')
    except models.LostFoundPosting.DoesNotExist:
        raise Exception("Empty lostfound") # no lost/found postings  
    # get rideshare
    try:
        rideshare = models.Rideshare.objects.filter(
            school__pk=request.user.get_profile().school.id,
            fulfilled=False
        ).order_by('-pub_date')
    except models.Rideshare.DoesNotExist:
        raise Exception("Empty rideshare") # no rideshares
        
    return render(request, "services.html", 
        {'help': help, 'lost_found': lost_found, 'rideshare': rideshare})
        
@login_required
def service_generator(request, template, model, pk, categories=None):
    if pk is None:
        posts = model.objects.filter(
            school__pk=request.user.get_profile().school.id,
            fulfilled=False
        )
        
        # get featured listings
        featured = model.objects.random(quantity=3)
        
        return render(request, template + ".html", {'listings': posts, 'categories': categories, 'featured_list': featured})
    else:
        post = get_object_or_404(model, id=pk, school__pk=request.user.get_profile().school.id)
        
        # mark notification as seen
        notices = notification.Notice.objects.notices_for(request.user, unseen=True)
        for notice in notices:
            if notice.notice_type.label == "new_comment":
                if notice.content_object.content_object == post:
                    notice.unseen = False
                    notice.save()
        
        # get featured listings
        featured = model.objects.random(quantity=3)
                
        return render(request, template + "_single.html", {'listing': post, 'featured_list': featured})

@login_required
def help_wanted(request, pk=None):
    return service_generator(request, "help_wanted", models.HelpWanted, pk)
        
@login_required
def lost_found(request, pk=None):
    # get category count
    categories = []
    for category in models.LostFoundCategory.objects.all():
        count = models.LostFoundPosting.filter(category=category).count()
        categories.append((category, count))

    category_id = request.GET.get('category', '')
    if category_id:
        listings = models.LostFoundPosting.objects.filter(
            category__pk=category_id, 
            school__pk=request.user.get_profile().school.id,
            fulfilled=False
        ).order_by('-pub_date')
        return render(request, "lost_found.html", {'listings': listings, 'categories': categories})
    else:
        return service_generator(request, "lost_found", models.LostFoundPosting, pk, categories=categories)    
        
@login_required
def rideshare(request, pk=None):
    return service_generator(request, "rideshare", models.Rideshare, pk)
        
@login_required
def my_life(request):
    listings, help, lost_found, rideshare = None, None, None, None
    # get market listings
    try:
        listings = models.Listing.objects.filter(
            owner__pk=request.user.id, fulfilled=False
        ).order_by('-pub_date')
    except models.Listing.DoesNotExist:
        pass
    # get help wanted postings
    try:
        help = models.HelpWanted.objects.filter(
            owner__pk=request.user.id, fulfilled=False
        ).order_by('-pub_date')
    except models.HelpWanted.DoesNotExist:
        pass
    # get lost & found
    try:
        lost_found = models.LostFoundPosting.objects.filter(
            owner__pk=request.user.id, fulfilled=False
        ).order_by('-pub_date')
    except models.LostFoundPosting.DoesNotExist:
        pass   
    # get rideshare
    try:
        rideshare = models.Rideshare.objects.filter(
            owner__pk=request.user.id, fulfilled=False
        ).order_by('pub_date')
    except models.Rideshare.DoesNotExist:
        pass
        
    # get notifications
    notices = notification.Notice.objects.notices_for(request.user, unseen=True)
    
    outbidded = notification.NoticeType.objects.get(label="outbidded")
    new_bid = notification.NoticeType.objects.get(label="new_bid")
    new_comment = notification.NoticeType.objects.get(label="new_comment")
    
    outbidded_count, new_bid_count, new_comment_count = 0, 0, 0
    
    # tally up notification types
    for notice in notices:
        if notice.notice_type == outbidded:
            outbidded_count += 1
        elif notice.notice_type == new_bid:
            new_bid_count += 1
        elif notice.notice_type == new_comment:
            new_comment_count += 1
            
    # get featured listings
    featured = models.Listing.objects.random(quantity=3)
        
    return render(request, "my_life.html", 
        {'listings': listings, 
         'help': help, 
         'lost_found': lost_found, 
         'rideshare': rideshare,
         'outbidded_count': outbidded_count,
         'new_bid_count': new_bid_count,
         'new_comment_count': new_comment_count,
         'featured_list': featured,
    })
        
                
@login_required
def delete_comment(request, pk):
    comment = get_object_or_404(comments.get_model(), pk=pk)
    listing = comment.content_object
    if request.user != listing.owner:
        return render(request, "short_message.html", 
            {'headline': 'Access Denied', 
            'message': 'You do not have permission to delete this comment.',
            }
        )
    comment.is_removed = True
    comment.save()
    return render(request, "short_message.html", 
        {'headline': 'Comment Deleted', 
        'message': 'The comment has been successfully deleted.',
        'backlink': reverse('market_single', args=[listing.id])}
    )

@login_required
def delete_listing(request, type, pk):
    listing = None
    if type == "listing":
        listing = models.Listing.objects.get(id=pk)
    elif type == "rideshare":
        listing = models.Rideshare.objects.get(id=pk)
    elif type == "lostfoundposting":
        listing = models.LostFoundPosting.objects.get(id=pk)
    elif type == "helpwanted":
        listing = models.HelpWanted.objects.get(id=pk)
    else:
        return render(request, "short_message.html", 
            {'headline': 'Invalid Type', 
            'message': 'You tried to delete a listing of an invalid type.',
            }
        )

    if request.user != listing.owner:
        return render(request, "short_message.html", 
            {'headline': 'Access Denied', 
            'message': 'You do not have permission to delete this listing.',
            }
        )
    listing.delete()    
    return render(request, "short_message.html", 
        {'headline': 'Listing Deleted', 
        'message': 'The listing has been successfully deleted.',
        }
    )
    
     
@login_required
def toggle_fulfilled(request, pk):
    listing = get_object_or_404(models.Listing, pk=pk)
    context = None
    if request.user == listing.owner:
        if listing.fulfilled:
            listing.fulfilled = False
            context = {'headline': 'Listing market unfulfilled', 
                       'message': "The listing is now marked as unfulfilled. \
                       Users can leave new bids or comments, and it will \
                       appear in the marketplace."}
        else: 
            listing.fulfilled = True
            context = {'headline': 'Listing fulfilled', 
                       'message': "The listing is now marked as fulfilled. \
                       Users can't leave new bids or comments, and it will \
                       no longer appear in the marketplace."}
        listing.save()
        context["backlink"] = reverse('market_single', args=[pk])
    return render(request, "short_message.html", context)