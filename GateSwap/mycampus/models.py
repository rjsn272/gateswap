from django.db import models
from GateSwap.collegemall.models import School

# Create your models here.
class Event(models.Model):
    title = models.CharField(max_length=400)
    school = models.ForeignKey(School)
    link = models.CharField(max_length=400)
    startdate = models.DateField()
    starttime = models.TimeField(blank=True, null=True)
    enddate = models.DateField()
    endtime = models.TimeField(blank=True, null=True)
    location = models.CharField(max_length=400)
    contact = models.CharField(max_length=400)
    description = models.CharField(max_length=4000)