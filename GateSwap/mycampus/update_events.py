#!/home/ksolan/gateswap.com/env/bin/python
import sys, os

# deal with path shit
cwd = os.getcwd()
sys.path.append('/home/ksolan/gateswap.com/')
sys.path.append('/home/ksolan/gateswap.com/GateSwap/')
os.environ["DJANGO_SETTINGS_MODULE"] = 'GateSwap.settings'

import urllib2, json, re
from datetime import datetime
from GateSwap.mycampus import models
from GateSwap.collegemall.models import School

######################################
# update_events.py
# 
# Retrieves calendar from colgate.edu and inserts events into the django database
######################################
    

########## SCRAPING COLGATE ###########

colgate = School.objects.get(name="Colgate University")

calendar = urllib2.urlopen(
        "http://calendar.colgate.edu/webcache/v1.0/jsonDays/31/list-json/no--filter/no--object.json"
    ).read()
    
# fix colgate's crappy misformatted json so it doesn't crash the script
calendar = re.sub(r'\\(?!(\"|\\|/|b|f|n|r|t|u))', '\\\\\\\\', calendar)

calendar = json.loads(calendar)['bwEventList']['events']

for event in calendar:
    try:
        models.Event.objects.get(title=event['summary'])
        continue
    except models.Event.DoesNotExist:
        pass
        
    starttime, endtime, startdate, enddate = None, None, None, None
    try:
        starttime = datetime.strptime(event['start']['datetime'], "%Y%m%dT%H%M%S")
        startdate = starttime
        endtime = datetime.strptime(event['end']['datetime'], "%Y%m%dT%H%M%S")
        enddate = endtime
    except ValueError:
        startdate = datetime.strptime(event['start']['datetime'], "%Y%m%d")
        enddate = datetime.strptime(event['end']['datetime'], "%Y%m%d")
    
    event_model = models.Event(
        title=event['summary'], 
        link=event['link'], 
        starttime=starttime, 
        endtime=endtime,
        startdate=startdate,
        enddate=enddate,
        school=colgate,
        location=event['location']['address'],
        contact=event['contact']['name'],
        description=event['description'],
    )
    event_model.save()
    
######## SCRAPING COLGATE DONE ########
