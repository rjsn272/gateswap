from datetime import datetime
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from GateSwap.mycampus.models import Event

@login_required
def mycampus_main(request):
    school = request.user.get_profile().school
    
    # get all events for user's school
    events = Event.objects.filter(school=school, startdate__gte=datetime.now()).order_by('startdate')
    
    return render(request, "mycampus_main.html", {'events': events, 'school': school})
    