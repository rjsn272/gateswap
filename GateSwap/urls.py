from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from GateSwap.collegemall.forms import LoginForm, SuggestSchoolForm
from GateSwap.collegemall.models import School
from GateSwap.admin import site as admin_site
from GateSwap import settings

# not needed, since we manually register everything for our custom admin site
admin.autodiscover()

# we build this extra context to pass to the login view (this may be bad, but so be it)
# this is the same as the 'home' view context
extra_context = {'suggest_form': SuggestSchoolForm, 'schools': School.objects.all()}

# IF YOU CHANGE THE 'name' PARAMETER, IT WILL SCREW UP LINKS IN ALL THE TEMPLATES. DO SO AT YOUR OWN RISK.
urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'GateSwap.collegemall.views.home', name='home'),
    url(r'^submitschool/', 'GateSwap.collegemall.views.suggest_new_school', name='suggest_new_school'),
    url(r'^market/', 'GateSwap.collegemall.views.market', name='market'),
    url(r'^services/', 'GateSwap.collegemall.views.services', name='services'),
    url(r'^mylistings/', 'GateSwap.collegemall.views.my_life', name='my_life'),    
    url(r'^createlisting/', 'GateSwap.collegemall.views.create_listing', name='create_listing'),
    url(r'^editlisting/(?P<type>\w+)/(?P<pk>\d+)/$', 'GateSwap.collegemall.views.edit_listing', name='edit_listing'),
    url(r'^listing/(?P<pk>\d+)/$', 'GateSwap.collegemall.views.market_single', name='market_single'),
    url(r'^helpwanted/$', 'GateSwap.collegemall.views.help_wanted', name='help_wanted'),    
    url(r'^helpwanted/(?P<pk>\d+)/$', 'GateSwap.collegemall.views.help_wanted', name='help_wanted'),
    url(r'^lostandfound/$', 'GateSwap.collegemall.views.lost_found', name='lost_found'),    
    url(r'^lostandfound/(?P<pk>\d+)/$', 'GateSwap.collegemall.views.lost_found', name='lost_found'),    
    url(r'^rideshare/$', 'GateSwap.collegemall.views.rideshare', name='rideshare'),    
    url(r'^rideshare/(?P<pk>\d+)/$', 'GateSwap.collegemall.views.rideshare', name='rideshare'),
    url(r'^deletecomment/(?P<pk>\d+)/$', 'GateSwap.collegemall.views.delete_comment', name='delete_comment'),
    url(r'^deletelisting/(?P<type>\w+)/(?P<pk>\d+)/$', 'GateSwap.collegemall.views.delete_listing', name='delete_listing'),
    url(r'^togglefulfilled/(?P<pk>\d+)/$', 'GateSwap.collegemall.views.toggle_fulfilled', name='toggle_fulfilled'),
    url(r'^accounts/login/', 'GateSwap.collegemall.views.login', {'template_name': 'welcome.html', 'authentication_form': LoginForm, 'extra_context': extra_context }, name='login'),
    url(r'^accounts/logout/', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    url(r'^mycampus/', include('GateSwap.mycampus.urls')),
    url(r'^register/', include('GateSwap.registration.backends.gateswap.urls')),
    url(r'^notification/', include('GateSwap.notification.urls')),
    url(r'^comments/', include('django.contrib.comments.urls')),
    # url(r'^GateSwap/', include('GateSwap.foo.urls')),

    # footer urls
    url(r'^about/', TemplateView.as_view(template_name="about.html"), {"extra_context": {"form": LoginForm}}, name='about'),
    url(r'^terms/', TemplateView.as_view(template_name="terms_conditions.html"), name='terms'),
    url(r'^privacy/', TemplateView.as_view(template_name="privacy_policy.html"), name='privacypolicy'),
    url(r'^contact/', TemplateView.as_view(template_name="contact_us.html"), name='contactus'),


    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # our custom admin
    url(r'^admin/', include(admin_site.urls)),
    # django standard admin
    #url(r'^admin/', include(admin.site.urls)),
)

# to handle MEDIA uploads
if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))