from django.contrib import admin
from GateSwap.admin import site

from notification.models import NoticeType, NoticeSetting, Notice, ObservedItem, NoticeQueueBatch


class NoticeTypeAdmin(admin.ModelAdmin):
    list_display = ["label", "display", "description", "default"]


class NoticeSettingAdmin(admin.ModelAdmin):
    list_display = ["id", "user", "notice_type", "medium", "send"]


class NoticeAdmin(admin.ModelAdmin):
    list_display = ["message", "recipient", "sender", "notice_type", "added", "unseen", "archived"]


site.register(NoticeQueueBatch)
site.register(NoticeType, NoticeTypeAdmin)
site.register(NoticeSetting, NoticeSettingAdmin)
site.register(Notice, NoticeAdmin)
site.register(ObservedItem)
