{% load i18n %}

{% blocktrans %}
Your bid on {{ listing.title }} was outbidded.

Your bid: ${{ old_amount }}
New bid: ${{ new_amount }}
Made by: {{ made_by.get_full_name }}, {{ made_by.email }}

Please follow this link to make a new bid: http://www.gateswap.com{% url GateSwap.collegemall.market_single listing.id %}
{% endblocktrans %}
