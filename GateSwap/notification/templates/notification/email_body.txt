{% load i18n %}
{% blocktrans %}
You have received the following notice from {{ current_site }}:

Dear Gateswap User,
{{ message }}

Thanks!
Gateswap.com
{% endblocktrans %}