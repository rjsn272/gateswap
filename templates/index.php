<?php $page = $_GET["page"]; ?>

<!doctype html>

<html>
	<head>
		<title>
			<?php 
				$titles = array(
					"welcome" => "Gateswap - Welcome",
					"register_school" => "Gateswap - Register (School)",
					"homepage" => "Gateswap - Homepage",
					"my_life" => "Gateswap - My Life",
					"my_campus" => "Gateswap - My Campus",
					"market" => "Gateswap - Market",
					"market_single" => "Gateswap - Market (Single)",
					"services" => "Gateswap - Services",
					"services_helpwanted" => "Gateswap - Help Wanted",
					"services_helpwanted_single" => "Gateswap - Help Wanted (Single)",
					"services_lostandfound" => "Gateswap - Lost and Found",
					"services_lostandfound_single" => "Gateswap - Lost and Found (Single)",
					"services_rideshare" => "Gateswap - Rideshare",
					"services_rideshare_single" => "Gateswap - Rideshare (Single)",
					"create_listing" => "Gateswap - Create Listing",
					"listing_admin" => "Gateswap - Listing Admin"
				);
				if (array_key_exists($page, $titles))
					echo $titles[$page];
				else 
					echo "Page not found!";
				$logged_in = ($page != "welcome" && $page != "register_school");
			?>		
		</title>
		<link rel="stylesheet" type="text/css" href="style.css" />
		<link rel="stylesheet" type="text/css" href="plugins.css" />
		<script type="text/javascript" src="jquery-1.8.0.min.js"></script>	
		<script type="text/javascript" src="scripts.js"></script>
		<script type="text/javascript" src="plugins.js"></script>			
	</head>
	<body>
		<div id="header" <?php if (!$logged_in) echo "class='home'"; ?>>
			<?php if ($logged_in): ?>
			<div id="navigation">
				<ul class="menu">
					<li class="menu_item">
						<span class="heading">My Life</span><span class="menu_arrow">&#x25BC;</span>
						<ul class="submenu">
							<li class="submenu_item">
								<a href="">My Listings</a>
							</li>
							<li class="submenu_item">
								<a href="">My Organizations</a>
							</li>
							<li class="submenu_item">
								<a href="">My Calendar</a>
							</li>
						</ul>
					</li>
					<li class="menu_item">
						<span class="heading">Market</span><span class="menu_arrow">&#x25BC;</span>
						<ul class="submenu">
							<li class="submenu_item">
								<a href="">Item 1</a>
							</li>
							<li class="submenu_item">
								<a href="">Item 2</a>
							</li>
							<li class="submenu_item">
								<a href="">Item 3</a>
							</li>
						</ul>
					</li>
					<li class="menu_item">
						<span class="heading">Organizations</span><span class="menu_arrow">&#x25BC;</span>
						<ul class="submenu">
							<li class="submenu_item">
								<a href="">Item 1</a>
							</li>
							<li class="submenu_item">
								<a href="">Item 2</a>
							</li>
							<li class="submenu_item">
								<a href="">Item 3</a>
							</li>
						</ul>
					</li>
					<li class="menu_item">
						<span class="heading">Services</span><span class="menu_arrow">&#x25BC;</span>
						<ul class="submenu">
							<li class="submenu_item">
								<a href="">Item 1</a>
							</li>
							<li class="submenu_item">
								<a href="">Item 2</a>
							</li>
							<li class="submenu_item">
								<a href="">Item 3</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
			<?php else: ?>
			<div class="caption">Avicii at Colgate's Spring Party Weekend 2012</div>
			<?php endif; ?>
			<div id="top_bar">
				<?php if ($logged_in): ?>
					<div class="welcome">
						<p>Welcome, William</p>
						<div class="notifications">2</div>
						<a href="">Logout</a>
					</div>
				<?php else: ?>
					<div class="login">
						<div class="register">
							<a class="fancybox fancybox.iframe" href="">Register</a>
						</div>
						<div class="form">
							<input type="text" title="Username" value="Username" />
							<input type="text" title="Password" value="Password" />
							<input type="submit" value="Login" />
							<a href="">Forgot password?</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<div id="title">
				<h1>Gate</h1>
				<h2>Swap</h2>
				<div class="subtitle">	
					<?php if (!$logged_in): ?>
					Building a Stronger campus
					<?php else: ?>
					@ Colgate University 
					<?php endif; ?>
				</div>
			</div>
			<?php if ($page != "register_school"): ?>
			<div id="header_box">
				<?php if ($page == "welcome"): ?>
					<div class="welcome_box">
						<div class="find">
							<h2>Find Your School</h2>
							<input type="text" title="College, university, etc." value="College, university, etc." />
						</div>
						<div class="vote">
							<h3>Not here?</h3>
							<p>If your school is not available, vote to bring GateSwap to your school.</p>
							<input type="text" title="College, university, etc." value="College, university, etc." /><input type="submit" value="Submit" />
						</div>
					</div>
				<?php else: ?>
					<div class="calendar_box">
						<input type="text" title="Search a keyword or ISBN#" value="Search a keyword or ISBN#" />
						<h2>Calendar of Events</h2>
						<div id="calendar">
							<p>
								<a href="">College Preparatory D.A.N.C.E Camp</a><br/>
								<span class="details">Huntington Gym - 7.4.12</span>
							</p>
							<p>
								<a href="">College Volleyball Elite Camp</a><br/>
								<span class="details">Huntington Gym - 7.3.12</span>
							</p>
							<p>
								<a href="">Annual Alumni Golf Tournament</a><br/>
								<span class="details">Huntington Gym - 7.3.12</span>
							</p>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>
		<div id="main">
			<div id="main_wrapper">
				<?php switch($page):
				case "welcome": ?>
					<div class="content_15 left">
						<h2>About GateSwap</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<iframe width="336" height="189" class="video" src="http://www.youtube.com/embed/reLjhGLSLso" frameborder="0" allowfullscreen></iframe>
						<p>Praesent et lorem nec quam porttitor congue. Morbi sit amet odio sit amet est ultricies consequat eget et augue. Fusce ornare hendrerit est, quis dignissim arcu facilisis id. Sed id leo quis velit cursus vestibulum condimentum ac ipsum. Etiam eleifend elit quis nulla adipiscing tincidunt. Sed sed dictum urna. Vivamus feugiat justo sit amet nibh aliquam in cursus felis pretium. Morbi iaculis, nisl in consequat adipiscing, libero velit eleifend mauris, non dignissim velit urna quis quam. Praesent fermentum nulla rutrum tortor tincidunt et sagittis sapien adipiscing. Quisque congue augue eget libero feugiat ut pellentesque lacus lobortis. Suspendisse vitae justo eu lectus sollicitudin cursus ut eget turpis. Donec in eros nisl. Aliquam sollicitudin nisi felis.</p>
						<p>Praesent accumsan ligula eget lacus auctor tempor. Praesent rhoncus, leo vel congue rutrum, est nisl congue nisi, id congue nisi sapien commodo neque. Ut erat dui, auctor eget malesuada in, cursus et ligula. Quisque iaculis imperdiet vestibulum. Proin condimentum tortor ut nibh dignissim eu consectetur odio consectetur. Quisque fringilla erat ut orci mattis ac scelerisque tellus pellentesque. Proin leo risus, faucibus eget vehicula in, rutrum aliquet magna. Nunc et neque mi, ut fringilla dui. Curabitur vitae consectetur libero. Cras enim felis, accumsan sit amet ultrices pulvinar, feugiat eget tortor. Integer ornare risus ut nisi semper eget luctus orci sollicitudin. Maecenas eu lorem magna, sit amet accumsan quam.</p>
					</div>
					<div class="content_9 right">
						<div class="top_rounded_box">
							<h2>What We Offer</h2>
							<h3>Your Marketplace</h3>
							<p>Praesent accumsan ligula eget lacus auctor tempor. Praesent rhoncus, leo vel congue rutrum, est nisl congue nisi, id congue nisi sapien commodo neque.</p>
							<h3>Your Organizations</h3>
							<p>Cras enim felis, accumsan sit amet ultrices pulvinar, feugiat eget tortor.</p>
							<h3>Find Services</h3>
							<p>Maecenas eu lorem magna, sit amet accumsan quam.</p>	
						</div>
					</div>
				<?php break; ?>
				<?php case "register_school": ?>
					<div class="content_14 left">
						<h2>So, you go to Colgate? Awesome!</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<p>Praesent et lorem nec quam porttitor congue. Morbi sit amet odio sit amet est ultricies consequat eget et augue. Fusce ornare hendrerit est, quis dignissim arcu facilisis id. Sed id leo quis velit cursus vestibulum condimentum ac ipsum. Etiam eleifend elit quis nulla adipiscing tincidunt. Sed sed dictum urna. Vivamus feugiat justo sit amet nibh aliquam in cursus felis pretium. Morbi iaculis, nisl in consequat adipiscing, libero velit eleifend mauris, non dignissim velit urna quis quam. Praesent fermentum nulla rutrum tortor tincidunt et sagittis sapien adipiscing. Quisque congue augue eget libero feugiat ut pellentesque lacus lobortis. Suspendisse vitae justo eu lectus sollicitudin cursus ut eget turpis. Donec in eros nisl. Aliquam sollicitudin nisi felis.</p>						
					</div>
					<div class="content_10 right">
						<div class="register_school top_rounded_box">
							<h2>Register Here</h2>
							<p><label>First Name:</label><input type="text" /></p>
							<p><label>Last Name:</label><input type="text" /></p>
							<p><label>School:</label><input type="text" /></p>
							<p><label>Email:</label><input type="text" /></p>
							<input type="submit" value="Sign me up" />
						</div>
					</div>
				<?php break; ?>
				<?php case "homepage": ?>
					<div class="content_15 left">
						<h2>Market</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<a href=""><div class="homepage_market_listing">
							<div class="main">
								<img src="images/ico_sell.png" />
								<div class="desc">
									<h4>Selling my leather couch - $150</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
								</div>
								<div class="contributor">
									Posted by Jill Smith<br/>
									7.3.12 - 1:15 PM
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div></a>
						<a href=""><div class="homepage_market_listing">
							<div class="main">
								<img src="images/ico_buy.png" />
								<div class="desc">
									<h4>Looking for a used kayak</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
								</div>
								<div class="contributor">
									Posted by Jill Smith<br/>
									7.3.12 - 1:15 PM
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div></a>						
						<a href=""><div class="homepage_market_listing">
							<div class="main">
								<img src="images/ico_sell.png" />
								<div class="desc">
									<h4>Selling my leather couch - $150</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
								</div>
								<div class="contributor">
									Posted by Jill Smith<br/>
									7.3.12 - 1:15 PM
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div></a>
						<a href=""><div class="homepage_market_listing">
							<div class="main">
								<img src="images/ico_buy.png" />
								<div class="desc">
									<h4>Looking for a used kayak</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
								</div>
								<div class="contributor">
									Posted by Jill Smith<br/>
									7.3.12 - 1:15 PM
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div></a>						
						<a href=""><div class="homepage_market_listing">
							<div class="main">
								<img src="images/ico_sell.png" />
								<div class="desc">
									<h4>Selling my leather couch - $150</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
								</div>
								<div class="contributor">
									Posted by Jill Smith<br/>
									7.3.12 - 1:15 PM
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div></a>
						<a href=""><div class="homepage_market_listing">
							<div class="main">
								<img src="images/ico_buy.png" />
								<div class="desc">
									<h4>Looking for a used kayak</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
								</div>
								<div class="contributor">
									Posted by Jill Smith<br/>
									7.3.12 - 1:15 PM
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div></a>						
					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>	
						<div class="rounded_box">
							<h2>Organizations</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							<div class="homepage_organizations">
								<img src="images/ico_residential.png" /><h3>Residential Life</h3>
								<img src="images/ico_government.png" /><h3>Student Government</h3>
								<img src="images/ico_administration.png" /><h3>Administration</h3>
							</div>
						</div>
						<div class="rounded_box">
							<h2>Services</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							<h3>Help Wanted</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							<h3>Lost + Found</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>			
						</div>						
					</div>
				<?php break; ?>
				<?php case "my_life": ?>
					<div class="content_15 left">
						<h2 class="blue">My Life</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>						
					</div>
					<div class="content_15 left clearfix">
						<h2 class="green inline">My Listings</h2>
						<div class="mylife_notification">1 new comment!</div>
						<div class="mylife_listing">
							<div class="icon">
								<img src="images/ico_sell.png" />
							</div>
							<div class="main">
								<h4>Gateswap Ticket - Need help moving 7.25</h4>
								<a href="" class="edit">Edit Listing</a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<div class="date">7.3.12 - 1:15 PM</div>
								<div class="comments_title">Comments</div>
								<div class="comment">
									<p><strong>Erin Smith</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<div class="desc">
										<div class="time">7.3.12 - 1:15 PM</div>
										<div class="respond"><a href="">Respond</a></div>
									</div>
								</div>
								<div class="comment">
									<p><strong>Erin Smith</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<div class="desc">
										<div class="time">7.3.12 - 1:15 PM</div>
										<div class="respond"><a href="">Respond</a></div>
									</div>
								</div>
								<div class="comment">
									<p><strong>Erin Smith</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<div class="desc">
										<div class="time">7.3.12 - 1:15 PM</div>
										<div class="respond"><a href="">Respond</a></div>
									</div>
								</div>								
							</div>
						</div>
						<div class="mylife_listing">
							<div class="icon">
								<img src="images/ico_buy.png" />
							</div>
							<div class="main">
								<h4>Gateswap Ticket - Need help moving 7.25</h4>
								<a href="" class="edit">Edit Listing</a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<div class="date">7.3.12 - 1:15 PM</div>
								<div class="comments_title">No Comments</div>							
							</div>
						</div>											
					</div>
					<div class="content_9 right">
					<!--
						<div class="mylife_featured_listing">
							<h2 class="green">Featured Listing</h2>
							<img src="images/example_item.png" />
							<a href=""><h4>Selling my used Canon SLR camera</h4></a>
							<strong>Asking $200</strong>
							<div class="details">
								July 25, 2012<br/>
								Jessica Taylor
							</div>							
						</div>
						<div class="mylife_top_listings">
							<h2 class="green">Top Listings</h2>
							<div class="listing">
								<div class="main">
									<a href=""><strong>Selling my leather couch</strong></a>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
								<div class="price">
									$200
								</div>
							</div>
							<div class="listing">
								<div class="main">
									<a href=""><strong>Selling my leather couch</strong></a>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
								<div class="price">
									$200
								</div>
							</div>
							<div class="listing">
								<div class="main">
									<a href=""><strong>Selling my leather couch</strong></a>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
								<div class="price">
									$200
								</div>
							</div>							
						</div>
					-->
						<h2 class="green">My Calendar</h2>
						<div class="mylife_side_box">
							<div class="item">
								<h4>The College Prepatory DANCE</h4>
								<p>Huntington Gym </p>
								<div class="small_text">7.3.12 - 1:15 PM</div>
							</div>
							<div class="item">
								<h4>The College Prepatory DANCE</h4>
								<p>Huntington Gym </p>
								<div class="small_text">7.3.12 - 1:15 PM</div>
							</div>							
							<div class="item">
								<h4>The College Prepatory DANCE</h4>
								<p>Huntington Gym </p>
								<div class="small_text">7.3.12 - 1:15 PM</div>
							</div>							
							<div class="item">
								<h4>The College Prepatory DANCE</h4>
								<p>Huntington Gym </p>
								<div class="small_text">7.3.12 - 1:15 PM</div>
							</div>
							<div class="item">
								<h4>The College Prepatory DANCE</h4>
								<p>Huntington Gym </p>
								<div class="small_text">7.3.12 - 1:15 PM</div>
							</div>
							<div class="item">
								<h4>The College Prepatory DANCE</h4>
								<p>Huntington Gym </p>
								<div class="small_text">7.3.12 - 1:15 PM</div>
							</div>
							<div class="item">
								<h4>The College Prepatory DANCE</h4>
								<p>Huntington Gym </p>
								<div class="small_text">7.3.12 - 1:15 PM</div>
							</div>
							<div class="item">
								<h4>The College Prepatory DANCE</h4>
								<p>Huntington Gym </p>
								<div class="small_text">7.3.12 - 1:15 PM</div>
							</div>
						</div>
						<h2 class="green">My Organizations</h2>
						<div class="mylife_side_box">
							<h4>Student Government</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<h4>Residential Life</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<h4>Administration</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<h4>Student Government</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<h4>Residential Life</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<h4>Administration</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>							
						</div>
					</div>
				<?php break; ?>
				<?php case "my_campus": ?>
				<?php break; ?>
				<?php case "market": ?>
					<div class="content_15 left">
						<h2>Market</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>	
					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>					
					</div>
					<div class="content_18 left clearfix">
						<div class="object_heading">
							<div class="title_desc">Title and Description</div>
							<div class="category_contact">Category and Seller</div>
							<div class="price_date">Price and Date</div>
						</div>
						<div class="object_single">
							<div class="icon"><img src="images/ico_help.png" /></div>
							<div class="title_desc">
								<a href=""><h4>Lost: Keys</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
							</div>
							<div class="category_contact">
								<h4>Furniture</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">no comments</a>							
							</div>
						</div>
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>						
						<div class="object_single">
							<div class="icon"><img src="images/ico_buy.png" /></div>
							<div class="title_desc">
								<h4>Found: Phone</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>								
							</div>
							<div class="category_contact">
								<h4>Sports</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>							
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">3 comments</a>
							</div>
						</div>						
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>								
						<div class="object_single">
							<div class="icon"><img src="images/ico_sell.png" /></div>
							<div class="title_desc">
								<h4>Selling my leather couch</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
							</div>
							<div class="category_contact">
								<h4>Furniture</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">no comments</a>							
							</div>
						</div>
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>						
						<div class="object_single">
							<div class="icon"><img src="images/ico_buy.png" /></div>
							<div class="title_desc">
								<h4>Looking for a used kayak</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>								
							</div>
							<div class="category_contact">
								<h4>Sports</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>							
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">3 comments</a>
							</div>
						</div>						
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>								
						<div class="object_single">
							<div class="icon"><img src="images/ico_sell.png" /></div>
							<div class="title_desc">
								<h4>Selling my leather couch</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
							</div>
							<div class="category_contact">
								<h4>Furniture</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">no comments</a>							
							</div>
						</div>
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>						
						<div class="object_single">
							<div class="icon"><img src="images/ico_buy.png" /></div>
							<div class="title_desc">
								<h4>Looking for a used kayak</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>								
							</div>
							<div class="category_contact">
								<h4>Sports</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>							
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">3 comments</a>
							</div>
						</div>						
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>								
						<div class="pagination">
							<a href="" class="prev"><div class="arrow">&#9664;</div>Previous</a>
							<a href="" class="active page">1</a>
							<a href="" class="page">2</a>
							<a href="" class="page">3</a>
							<a href="" class="page">4</a>
							<a href="" class="next">Next<div class="arrow">&#9654;</div></a>		
						</div>
					</div>
					<div class="content_6 right">
						<div class="market_featured">
							<h2>Featured</h2>
							<a href="">
								<img src="images/example_item.png" /><br/>
								<h4>Selling my used Old Town Kayak</h4>
							</a>
							<div class="asking">Asking $200</div>
							<div class="details">
								July 25, 2012<br/>Jessica Taylor
							</div>
						</div>
						<div class="object_categories">
							<h2>Categories</h2>
							<a href="">Books</a>
							<a href="">Electronics</a>
							<a href="">Furniture</a>
							<a href="">Art</a>
							<a href="">Clothing & Accessories</a>
							<a href="">Music</a>
							<a href="">Sports</a>
							<a href="">Tickets</a>
							<a href="">Everything Else</a>						
						</div>
					</div>
				<?php break; ?>
				<?php case "market_single": ?>
					<div class="content_15 left">
						<h2>Market</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<div class="single_listing_top">
							<div class="title">
								<h4>Title:</h4><br/>
								<h2 class="blue">Selling Leather Couch</h2>								
							</div>		
							<div class="icon">
								<img src="images/ico_sell.png" />
							</div>
							<div class="main">
								<div class="left">
									<div class="category">
										<h4>Category:</h4> Furniture
									</div>	
								</div>
								<div class="right">
									<div class="date">
										<h4>Date:</h4> July 23, 2012
									</div>	
								</div>
								<img src="images/example_item.png" />
								<div class="left">
									<div class="about">
										<h4>About/description:</h4><br/>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									</div>
									<div class="contact">
										<h4>Contact information:</h4><br/>
										Jessica Taylor<br/>
										jessica@gmail.com
									</div>									
								</div>
								<div class="right">
									<div class="price">
										<h4>Asking Price:</h4> <strong class="orange">$150</strong><br/>
										<h4>Highest Bid:</h4> <strong class="blue">$150</strong><br/>
										<h4>Your Bid:</h4> 
										<div class="your_bid">
											<input type="text" title="My bid is ..." value="My bid is ..." /><br/>
											<input type="submit" value="Place a Bid" />
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>									
						<div class="sidebar_comments">
							<h2 class="blue">Comments</h2><div class="comment_count">3</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>
							</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>							
							</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>							
							</div>						
						</div>
						<div class="sidebar_add_comment">
							<h2>Add Comment</h2>
							<textarea title="Start typing here ...">Start typing here ...</textarea>
							<input type="submit" value="submit" />
						</div>
											
					</div>				
				<?php break; ?>
				<?php case "services": ?>
					<div class="content_14 left">
						<h2>Services</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="content_10 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>	
					</div>
					<div class="content_8 left clearfix">
						<h2 class="green">Help Wanted</h2>
						<div class="services">
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comments"><a href="">2 comments</a></div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>							
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comments"><a href="">2 comments</a></div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>							
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comments"><a href="">2 comments</a></div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>							
						</div>
						<div class="services_view_all">
							<a href="">view all listings</a>
						</div>
					</div>
					<div class="content_8 left">
						<h2 class="green">Rideshare</h2>
						<div class="services">
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comments"><a href="">2 comments</a></div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>							
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comments"><a href="">2 comments</a></div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>							
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comments"><a href="">2 comments</a></div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>													
						</div>
						<div class="services_view_all">
							<a href="">view all listings</a>
						</div>						
					</div>
					<div class="content_8 right">
						<h2 class="green">Lost & Found</h2>
						<div class="services">
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comments"><a href="">2 comments</a></div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>							
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comments"><a href="">2 comments</a></div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>							
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comments"><a href="">2 comments</a></div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>
							<div class="listing">
								<a href=""><h4>Need help moving 7.25</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<div class="details">
									Posted by Jill Smith<br/>7.3.12 - 1:15 PM
								</div>
								<div class="comment_link"><a href="">Comment</a></div>
								<div class="clearfix"></div>
							</div>							
						
						</div>
						<div class="services_view_all">
							<a href="">view all listings</a>
						</div>						
					</div>
				<?php break; ?>
				<?php case "services_helpwanted": ?>
					<div class="content_15 left">
						<h2>Services: Help Wanted</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>	
					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>						
					</div>
					<div class="content_12 left clearfix">
						<div class="help_wanted_featured">
							<div class="top">
								<h2 class="green">Help Wanted</h2>
								<h3>Looking for Math Tutor</h3>
							</div>
							<div class="left">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<a href="">no comments</a>
							</div>
							<div class="right">
								<h4>When: </h4> August 25, 2012<br/>
								<h4>Where: </h4> Library<br/>
								<h4>How much: </h4> $10<br/>
							</div>
						</div>
					</div>
					<div class="content_12 right">
						<div class="help_wanted_featured">
							<div class="top">
								<h2 class="green">Help Wanted</h2>
								<h3>Looking for Math Tutor</h3>
							</div>
							<div class="left">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<a href="">3 comments</a>
							</div>
							<div class="right">
								<h4>When: </h4> August 25, 2012<br/>
								<h4>Where: </h4> Library<br/>
								<h4>How much: </h4> $10<br/>
							</div>
						</div>
					</div>
					<div class="content_24" clearfix">
						<div class="full_width_heading">
							<div class="title_desc">Title and Description</div>
							<div class="detail_1">When</div>
							<div class="detail_2">Where</div>
							<div class="detail_3">Compensation</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_help.png" />
								<div class="title_desc">
									<h4><a href="">Want to learn how to sail</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Anytime</div>
								<div class="detail_2">Library</div>
								<div class="detail_3">FREE</div>
								<div class="contact">
									<strong>$20</strong><br/>
									<a href="">no comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_help.png" />
								<div class="title_desc">
									<h4><a href="">Looking for math tutor</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">August 25, 2012</div>
								<div class="detail_2">Library</div>
								<div class="detail_3">$15</div>
								<div class="contact">
									<strong>---</strong><br/>
									<a href="">2 comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_help.png" />
								<div class="title_desc">
									<h4><a href="">Want to learn how to sail</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Anytime</div>
								<div class="detail_2">Library</div>
								<div class="detail_3">FREE</div>
								<div class="contact">
									<strong>$20</strong><br/>
									<a href="">no comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_help.png" />
								<div class="title_desc">
									<h4><a href="">Looking for math tutor</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">August 25, 2012</div>
								<div class="detail_2">Library</div>
								<div class="detail_3">$15</div>
								<div class="contact">
									<strong>---</strong><br/>
									<a href="">2 comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_help.png" />
								<div class="title_desc">
									<h4><a href="">Want to learn how to sail</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Anytime</div>
								<div class="detail_2">Library</div>
								<div class="detail_3">FREE</div>
								<div class="contact">
									<strong>$20</strong><br/>
									<a href="">no comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_help.png" />
								<div class="title_desc">
									<h4><a href="">Looking for math tutor</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">August 25, 2012</div>
								<div class="detail_2">Library</div>
								<div class="detail_3">$15</div>
								<div class="contact">
									<strong>---</strong><br/>
									<a href="">2 comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_help.png" />
								<div class="title_desc">
									<h4><a href="">Want to learn how to sail</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Anytime</div>
								<div class="detail_2">Library</div>
								<div class="detail_3">FREE</div>
								<div class="contact">
									<strong>$20</strong><br/>
									<a href="">no comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_help.png" />
								<div class="title_desc">
									<h4><a href="">Looking for math tutor</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">August 25, 2012</div>
								<div class="detail_2">Library</div>
								<div class="detail_3">$15</div>
								<div class="contact">
									<strong>---</strong><br/>
									<a href="">2 comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>												
					</div>
				<?php break; ?>
				<?php case "services_helpwanted_single": ?>
					<div class="content_15 left">							
						<h2>Services: Help Wanted</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<div class="single_listing_top">
							<div class="title">
								<h4>Title:</h4><br/>
								<h2 class="blue">Help Wanted: Math Tutor</h2>								
							</div>
							<div class="icon">
								<img src="images/ico_help.png" />
							</div>
							<div class="main">
								<div class="left">
									<div class="category">
										<h4>Category:</h4> Need a ride
									</div>
									<div class="about">
										<h4>About/description:</h4><br/>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									</div>
								</div>
								<div class="right">
									<div class="date">
										<h4>Date:</h4> July 23, 2012
									</div>								
									<div class="contact">
										<h4>Contact information:</h4><br/>
										Jessica Taylor<br/>
										jessica@gmail.com
									</div>
								</div>
							</div>
						</div>
						
						<div class="services_comments clearfix">
							<h2 class="orange">Comments</h2><div class="comment_count">3</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>
							</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>							
							</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>							
							</div>
						</div>
						<div class="services_add_comment">
							<h2>Add Comment</h2>
							<textarea title="Start typing here ...">Start typing here ...</textarea>
							<input type="submit" value="Submit" />
						</div>						
					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>						
						<div class="also_of_interest">
							<h2>May Also Be of Interest</h2>
							<div class="item">
								<h4>Math Tutor</h4>
								<div class="details">
									When: Before Sept. 1st<br/>
									Where: Library
								</div>
								<div class="comments_and_price">
									<div class="comments"><a href="">no comments</a></div>
									<div class="price">$20</div>
								</div>
							</div>
							<div class="item">
								<h4>Need help moving a couch</h4>							
								<div class="details">
									When: Before Sept. 1st<br/>
									Where: New York City
								</div>
								<div class="comments_and_price">
									<div class="comments"><a href="">no comments</a></div>
									<div class="price">$20</div>
								</div>
							</div>
							<div class="item">
								<h4>Writing an art history paper</h4>							
								<div class="details">
									When: Before Sept. 1st<br/>
									Where: Hamilton Ave.
								</div>
								<div class="comments_and_price">
									<div class="comments"><a href="">no comments</a></div>
									<div class="price">FREE</div>
								</div>
							</div>
						</div>
					</div>
				<?php break; ?>
				<?php case "services_lostandfound": ?>
					<div class="content_15 left">
						<h2>Services: Lost and Found</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>	
					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>					
					</div>
					<div class="content_18 left clearfix">
						<div class="object_heading">
							<div class="title_desc">Title and Description</div>
							<div class="category_contact">Category/Contact</div>
							<div class="price_date">Reward and Date</div>
						</div>					
						<div class="object_single">
							<div class="icon"><img src="images/ico_help.png" /></div>
							<div class="title_desc">
								<a href=""><h4>Lost: Keys</h4></a>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
							</div>
							<div class="category_contact">
								<h4>Furniture</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">no comments</a>							
							</div>
						</div>
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>
						<div class="object_single">
							<div class="icon"><img src="images/ico_buy.png" /></div>
							<div class="title_desc">
								<h4>Found: Phone</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>								
							</div>
							<div class="category_contact">
								<h4>Sports</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>							
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">3 comments</a>
							</div>
						</div>						
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>								
						<div class="object_single">
							<div class="icon"><img src="images/ico_help.png" /></div>
							<div class="title_desc">
								<h4>Lost: Keys</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
							</div>
							<div class="category_contact">
								<h4>Furniture</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">no comments</a>							
							</div>
						</div>
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>						
						<div class="object_single">
							<div class="icon"><img src="images/ico_buy.png" /></div>
							<div class="title_desc">
								<h4>Found: Phone</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>								
							</div>
							<div class="category_contact">
								<h4>Sports</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>							
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">3 comments</a>
							</div>
						</div>						
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>								
						<div class="object_single">
							<div class="icon"><img src="images/ico_help.png" /></div>
							<div class="title_desc">
								<h4>Lost: Keys</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
							</div>
							<div class="category_contact">
								<h4>Furniture</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">no comments</a>							
							</div>
						</div>
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>						
						<div class="object_single">
							<div class="icon"><img src="images/ico_buy.png" /></div>
							<div class="title_desc">
								<h4>Found: Phone</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>								
							</div>
							<div class="category_contact">
								<h4>Sports</h4>
								<p>Jessica Taylor<br/>jessica@gmail.com</p>							
							</div>
							<div class="price_date">
								<h3>$150</h3>
								<h4>July 23, 2012</h4>
								<a href="">3 comments</a>
							</div>
						</div>						
						<div class="object_single_image">
							<img src="images/example_item.png" />
						</div>								
						<div class="pagination">
							<a href="" class="prev"><div class="arrow">&#9664;</div>Previous</a>
							<a href="" class="active page">1</a>
							<a href="" class="page">2</a>
							<a href="" class="page">3</a>
							<a href="" class="page">4</a>
							<a href="" class="next">Next<div class="arrow">&#9654;</div></a>		
						</div>
					</div>
					<div class="content_6 right">
						<div class="object_categories">
							<h2>Categories</h2>
							<a href="">Phones</a>
							<a href="">ID's</a>
							<a href="">Clothes</a>
							<a href="">Keys</a>
							<a href="">Electronics</a>
							<a href="">Miscellaneous</a>
						</div>
					</div>
				<?php break; ?>
				<?php case "services_lostandfound_single": ?>
					<div class="content_15 left">	`					
						<h2>Services: Lost and Found</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<div class="single_listing_top">
							<div class="title">
								<h4>Title:</h4><br/>
								<h2 class="blue">Lost: iPod at the Library</h2>								
							</div>		
							<div class="icon">
								<img src="images/ico_help.png" />
							</div>
							<div class="main">
								<div class="left">
									<div class="category">
										<h4>Category:</h4> Electronics
									</div>
								</div>
								<div class="right">
									<div class="date">
										<h4>Date:</h4> July 23, 2012
									</div>	
								</div>
								<img src="images/example_item.png" />
								<div class="left">
									<div class="about">
										<h4>About/description:</h4><br/>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									</div>
								</div>
								<div class="right">
									<div class="price">
										<h4>Reward:</h4> <strong class="orange">$150</strong>
									</div>
									<div class="contact">
										<h4>Contact information:</h4><br/>
										Jessica Taylor<br/>
										jessica@gmail.com
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>		
						<div class="sidebar_comments">
							<h2 class="blue">Comments</h2><div class="comment_count">3</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>
							</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>							
							</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>							
							</div>						
						</div>
						<div class="sidebar_add_comment">
							<h2>Add Comment</h2>
							<textarea title="Start typing here ...">Start typing here ...</textarea>
							<input type="submit" value="submit" />
						</div>
					</div>				
				<?php break; ?>
				<?php case "services_rideshare": ?>
					<div class="content_15 left">
						<h2>Services: Rideshare</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>							
					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>						
					</div>
					<div class="content_24 clearfix">
						<div class="full_width_heading">
							<div class="title_desc">Type</div>
							<div class="detail_1">To</div>
							<div class="detail_2">From</div>
							<div class="detail_3">When</div>
							<div class="detail_4">Price and Contact</div>
						</div>					
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_rideshare_find.png" />
								<div class="title_desc">
									<h4><a href="">Need a Ride</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Syracuse, NY</div>
								<div class="detail_2">Hamilton, NY</div>
								<div class="detail_3">July 23, 2012</div>
								<div class="contact">
									<strong>$20</strong><br/>
									<a href="">no comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_rideshare.png" />
								<div class="title_desc">
									<h4><a href="">Giving a Ride</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Syracuse, NY</div>
								<div class="detail_2">Hamilton, NY</div>
								<div class="detail_3">July 23, 2012</div>
								<div class="contact">
									<strong>---</strong><br/>
									<a href="">2 comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>	
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_rideshare_find.png" />
								<div class="title_desc">
									<h4><a href="">Need a Ride</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Syracuse, NY</div>
								<div class="detail_2">Hamilton, NY</div>
								<div class="detail_3">July 23, 2012</div>
								<div class="contact">
									<strong>$20</strong><br/>
									<a href="">no comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_rideshare.png" />
								<div class="title_desc">
									<h4><a href="">Giving a Ride</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Syracuse, NY</div>
								<div class="detail_2">Hamilton, NY</div>
								<div class="detail_3">July 23, 2012</div>
								<div class="contact">
									<strong>---</strong><br/>
									<a href="">2 comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>	
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_rideshare_find.png" />
								<div class="title_desc">
									<h4><a href="">Need a Ride</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Syracuse, NY</div>
								<div class="detail_2">Hamilton, NY</div>
								<div class="detail_3">July 23, 2012</div>
								<div class="contact">
									<strong>$20</strong><br/>
									<a href="">no comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_rideshare.png" />
								<div class="title_desc">
									<h4><a href="">Giving a Ride</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Syracuse, NY</div>
								<div class="detail_2">Hamilton, NY</div>
								<div class="detail_3">July 23, 2012</div>
								<div class="contact">
									<strong>---</strong><br/>
									<a href="">2 comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>	
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_rideshare_find.png" />
								<div class="title_desc">
									<h4><a href="">Need a Ride</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Syracuse, NY</div>
								<div class="detail_2">Hamilton, NY</div>
								<div class="detail_3">July 23, 2012</div>
								<div class="contact">
									<strong>$20</strong><br/>
									<a href="">no comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>
						<div class="full_width_listing">
							<div class="main">
								<img src="images/ico_rideshare.png" />
								<div class="title_desc">
									<h4><a href="">Giving a Ride</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
								</div>
								<div class="detail_1">Syracuse, NY</div>
								<div class="detail_2">Hamilton, NY</div>
								<div class="detail_3">July 23, 2012</div>
								<div class="contact">
									<strong>---</strong><br/>
									<a href="">2 comments</a>
								</div>
							</div>
							<div class="arrow">&#9654;</div>
						</div>																			
					</div>
				<?php break; ?>
				<?php case "services_rideshare_single": ?>
					<div class="content_15 left">					
						<h2>Services: Rideshare</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<div class="single_listing_top">
							<div class="title">
								<h4>Title:</h4><br/>
								<h2 class="blue">Giving A Ride: To Syracuse</h2>								
							</div>
							<div class="icon">
								<img src="images/ico_rideshare.png" />
							</div>
							<div class="main">
								<div class="left">
									<div class="category">
										<h4>Category:</h4> Need a ride
									</div>
									<div class="about">
										<h4>About/description:</h4><br/>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									</div>	
								</div>
								<div class="right">
									<div class="date">
										<h4>Date:</h4> July 23, 2012
									</div>								
									<div class="contact">
										<h4>Contact information:</h4><br/>
										Jessica Taylor<br/>
										jessica@gmail.com
									</div>
								</div>
							</div>
						</div>
						<div class="services_comments clearfix">
							<h2 class="orange">Comments</h2><div class="comment_count">3</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>
							</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>							
							</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="details">
									7.3.12 - 1:15 PM
								</div>							
							</div>
						</div>
						<div class="services_add_comment">
							<h2>Add Comment</h2>
							<textarea title="Start typing here ...">Start typing here ...</textarea>
							<input type="submit" value="Submit" />
						</div>
					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Create A Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>													
						<div class="also_of_interest">
							<h2>May Also Be of Interest</h2>
							<div class="item">								
								<div class="details">
									<h4>Giving a Ride</h4>
									<strong>To:</strong> New York City<br/>
									<strong>From:</strong> New York City
								</div>
								<div class="comments_and_price">
									<h4>July 23, 2012</h4>
									<div class="comments"><a href="">no comments</a></div>
									<div class="price">$20</div>
								</div>
							</div>
							<div class="item">
								<div class="details">
									<h4>Giving a Ride</h4>
									<strong>To:</strong> New York City<br/>
									<strong>From:</strong> New York City
								</div>
								<div class="comments_and_price">
									<h4>July 23, 2012</h4>
									<div class="comments"><a href="">no comments</a></div>
									<div class="price">$20</div>
								</div>
							</div>
							<div class="item">
								<div class="details">
									<h4>Giving a Ride</h4>
									<strong>To:</strong> New York City<br/>
									<strong>From:</strong> New York City
								</div>
								<div class="comments_and_price">
									<h4>July 23, 2012</h4>
									<div class="comments"><a href="">no comments</a></div>
									<div class="price">$20</div>
								</div>
							</div>
						</div>
					</div>				
				<?php break; ?>				
				<?php case "create_listing": ?>
				<?php break; ?>
				<?php case "listing_admin": ?>
					<div class="content_15 left">
						<div class="listing_admin">
							<h2>Market</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							<div class="title">
								<h2 class="green">Title</h2>
								<h3>Selling Leather Couch</h3>
							</div>
							<div class="sold">
								<h2 class="green">Sold?</h2>
								<div class="checkbox">
								  <input id="check" type="checkbox" value="1" />
								  <label for="check">&nbsp;&#10004;</label>
								</div>
							</div>
							<img src="images/example_item.png" />
							<h2 class="green">About / Description</h2>			
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							<h3>Your Asking Price:</h3><div class="asking">$50</div>
							<h2 class="green">Bids</h2>
							<div class="bids">
								<div class="individual_bid">
									<div class="price highest">$100</div>
									<div class="details">
										Made by Kyle Solan on 7.3.12 at 1:15 PM
									</div>
									<a href="">Reply to bid</a>
								</div>
								<div class="individual_bid">
									<div class="price">$96</div>
									<div class="details">
										Made by Kyle Solan on 7.3.12 at 1:15 PM
									</div>
									<a href="">Reply to bid</a>									
								</div>
								<div class="individual_bid">
									<div class="price">$30</div>
									<div class="details">
										Made by Kyle Solan on 7.3.12 at 1:15 PM
									</div>
									<a href="">Reply to bid</a>									
								</div>
							</div>
						</div>
					</div>
					<div class="content_9 right">
						<a href=""><div class="create_listing_button">
							<div class="main">Edit Listing</div>
							<div class="arrow">&#9654;</div>
						</div></a>
						<div class="sidebar_comments">
							<h2 class="blue">Comments</h2><div class="comment_count">3</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="admin_options">
									<div class="details">
										7.3.12 - 1:15 PM
									</div>
									<a href="" class="reply">
										Reply via email
									</a>									
									<a href="" class="delete">
										Delete
									</a>
								</div>
							</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="admin_options">
									<div class="details">
										7.3.12 - 1:15 PM
									</div>
									<a href="" class="reply">
										Reply via email
									</a>									
									<a href="" class="delete">
										Delete
									</a>
								</div>			
							</div>
							<div class="comment">
								<h4>Eric Smith</h4> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								<div class="admin_options">
									<div class="details">
										7.3.12 - 1:15 PM
									</div>
									<a href="" class="reply">
										Reply via email
									</a>									
									<a href="" class="delete">
										Delete
									</a>
								</div>						
							</div>						
						</div>
						<div class="sidebar_add_comment">
							<h2>Add Comment</h2>
							<textarea title="Start typing here ...">Start typing here ...</textarea>
							<input type="submit" value="submit" />
						</div>	
					</div>
				<?php break; ?>				
				<?php default: ?>
				<?php endswitch; ?>
			</div>
		</div>
		<div id="footer">
			<div id="footer_wrapper">
				<div class="left">
					Copyright GateSwap 2012
				</div>
				<div class="right">
					<a href="">About</a>
					<a href="">Sitemap</a>
					<a href="">Terms + Conditions</a>
					<a href="">Contact</a>
				</div>
			</div>
		</div>
	</body>
</html>