Hi there!

You're one step away from getting access to Gateswap! For the final step, please click the link below to confirm your email address and activate your account:

http://www.gateswap.com{% url registration_activate activation_key %}

If the hyperlink doesn't work, you can copy and paste the URL in your web browser instead. Don't wait too long, as this link will expire in {{ expiration_days }} days!

Thanks a bunch,
Gateswap