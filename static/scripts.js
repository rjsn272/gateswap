$(document).ready(function() {

	// Navigation menu
	$("li.menu_item").hover(function() { 
		$(this).find('ul').slideDown("fast"); 
		$(this).find('.heading').css("color", "#62DF2D"); 
		$(this).find('.menu_arrow').css("opacity", "1"); 
	}, function() { 
		$(this).find('ul').hide();
		$(this).find('.heading').css("color", "#FFF"); 
		$(this).find('.menu_arrow').css("opacity", "0"); 
	});

	// Clears input fields when clicked
	$("input[type='text'], textarea").focus(function() {
		if (this.value == this.title) {
			$(this).val("");
			$(this).css("color", "#000");
		}
	}).blur(function () {
		if (this.value == "") {
			$(this).css("color", "#A8A8A8");
			$(this).val(this.title);
		}
	});
	
	// Pretty scrollbars!
	$(function()
	{
		$('#calendar').jScrollPane({showArrows: true, hideTrack: true});
		$('.services, .mylife_calendar').jScrollPane({showArrows: true, verticalGutter: 30});
	});	
	
	// Register bubble
	$(".fancybox").fancybox({
		maxWidth: 600,
		maxHeight: 650,
	});
	
	// Dropdown for Create Listing page
	$(".createlisting_select").hover(
		function() { $("ul.createlisting_select_options").slideDown("fast"); },
		function() { $("ul.createlisting_select_options").slideUp("fast"); }
	);
	$(".createlisting_option").click(function() {
		var id = $(this).attr("id");
		$(".createlisting_form").css("display", "none");
		$("#" + id + "_form").fadeIn();
	});
});				